from django.db import models
from django.utils.translation import gettext_lazy as _
from core.models import ContractCore
from phonenumber_field.modelfields import PhoneNumberField

class Member(models.Model):
    name   = models.CharField(
        max_length=120,
        verbose_name= _('name'),
        null=True, blank=False,
    )
    number = models.CharField(
        max_length=100,
        verbose_name= _('Membership number/ID'),
        null=True, blank=False,
        db_index=True,
    )
    member_uuid = models.IntegerField(
        verbose_name= _('member_uuid'),
        null=True, blank=False,
    )
    active = models.BooleanField()
    first_name = models.CharField(
        max_length=120,
        verbose_name= _('first name'),
        null=True, blank=False,
    )
    last_name = models.CharField(
        max_length=120,
        verbose_name= _('last name'),
        null=True, blank=False,
    )
    salutation = models.CharField(
        max_length=120,
        verbose_name= _('salutation'),
        null=True, blank=True,
    )
    form_of_address = models.CharField(
        max_length=60,
        verbose_name= _('Sir / Madan'),
        null=True, blank=True,
    )
    street = models.CharField(
        max_length=240,
        verbose_name= _('street with number'),
        null=True, blank=False,
    )
    zip = models.IntegerField(
        verbose_name= _('ZIP'),
        null=True, blank=False,
    )
    city = models.CharField(
        max_length=120,
        verbose_name= _('city'),
        null=True, blank=False,
    )
    country = models.CharField(
        max_length=120,
        verbose_name= _('country'),
        null=True, blank=False,
    )
    day_of_birth = models.DateField(
        verbose_name= _('day of birth'),
        null=True, blank=True,
    )
    phone_number = PhoneNumberField(
        verbose_name= _('call number'),
        null=True, blank=True,
    )
    cell_number = PhoneNumberField(
        verbose_name= _('cell phone number'),
        null=True, blank=True,
    )
    email = models.EmailField(
        max_length=120,
        verbose_name= _('email'),
        null=True, blank=False,
    )
    fax_number = PhoneNumberField(
        null=True, blank=True
    )
    payment_method = models.IntegerField(
        verbose_name= _('payment method'), # 1 direct debit # 2 cash
        null=True, blank=False,
    )
    iban = models.CharField(
        max_length=34,
        verbose_name= _('IBAN'),
        null=True, blank=True,
    )
    bic = models.CharField(
        max_length=11,
        verbose_name= _('BIC'),
        null=True, blank=True,
    )
    name_of_bank = models.CharField(
        max_length=120,
        verbose_name= _('name of bank'),
        null=True, blank=True,
    )
    mandate_reference = models.CharField(
        max_length=30,
        verbose_name= _('mandate reference'),
        null=True, blank=True,
    )
    # different_account_holder = models.BooleanField()
    account_holder = models.CharField(
        max_length=240,
        verbose_name= _('account holder'),
        null=True, blank=True,
    )
    entry_date = models.DateField(
        verbose_name= _('entry date'),
        null=True, blank=False,
    )
    exit_date = models.DateField(
        verbose_name= _('exit date'),
        null=True, blank=True
    )
    memo = models.TextField(
        verbose_name= _('memo'),
        null=True, blank=True,
    )




class Deleting_Member(models.Model):
    name   = models.CharField(
        max_length=120,
        verbose_name= _('Name'),
        null=True, blank=False,
    )
    number = models.CharField(
        max_length=100,
        verbose_name= _('Membership number/ID'),
        null=True, blank=False,
        db_index=True,
    )
    member_uuid = models.IntegerField(
        verbose_name= _('member_uuid'),
        null=True, blank=False,
    )
    active = models.BooleanField()
    first_name = models.CharField(
        max_length=120,
        verbose_name= _('first name'),
        null=True, blank=False,
    )
    last_name = models.CharField(
        max_length=120,
        verbose_name= _('last name'),
        null=True, blank=False,
    )
    salutation = models.CharField(
        max_length=120,
        verbose_name= _('salutation'),
        null=True, blank=True,
    )
    form_of_address = models.CharField(
        max_length=60,
        verbose_name= _('Sir / Madan'),
        null=True, blank=True,
    )
    street = models.CharField(
        max_length=240,
        verbose_name= _('street with number'),
        null=True, blank=False,
    )
    zip = models.IntegerField(
        verbose_name= _('ZIP'),
        null=True, blank=False,
    )
    city = models.CharField(
        max_length=120,
        verbose_name= _('city'),
        null=True, blank=False,
    )
    country = models.CharField(
        max_length=120,
        verbose_name= _('country'),
        null=True, blank=False,
    )
    day_of_birth = models.DateField(
        verbose_name= _('day of birth'),
        null=True, blank=True,
    )
    phone_number = PhoneNumberField(
        verbose_name= _('call number'),
        null=True, blank=True,
    )
    cell_number = PhoneNumberField(
        verbose_name= _('cell phone number'),
        null=True, blank=True,
    )
    email = models.EmailField(
        max_length=120,
        verbose_name= _('email'),
        null=True, blank=False,
    )
    fax_number = PhoneNumberField(
        verbose_name = _('telefax number'),
        null=True, blank=True
    )
    payment_method = models.IntegerField(
        verbose_name= _('payment method'),
        null=True, blank=False,
    )
    iban = models.CharField(
        max_length=34,
        verbose_name= _('IBAN'),
        null=True, blank=True,
    )
    bic = models.CharField(
        max_length=11,
        verbose_name= _('BIC'),
        null=True, blank=True,
    )
    name_of_bank = models.CharField(
        max_length=120,
        verbose_name= _('name of bank'),
        null=True, blank=True,
    )
    mandate_reference = models.CharField(
        max_length=30,
        verbose_name= _('mandate reference'),
        null=True, blank=True,
    )
    # different_account_holder = models.BooleanField()
    account_holder = models.CharField(
        max_length=240,
        verbose_name= _('account holder'),
        null=True, blank=True,
    )
    entry_date = models.DateField(
        verbose_name= _('entry date'),
        null=True, blank=False,
    )
    exit_date = models.DateField(
        verbose_name= _('exit date'),
        null=True, blank=True
    )
    memo = models.TextField(
        verbose_name= _('memo'),
        null=True, blank=True,
    )
    edit_flag = models.CharField(
        max_length=1,
        verbose_name= _('edit flag'),
        null=True, blank=False,
    )
    edit_date = models.DateField(
        verbose_name= _('edit date'),
        auto_now_add=True,
        editable=True, blank=True
    )

class Contract(models.Model):
    member = models.ForeignKey(
        Member, on_delete=models.SET_NULL, null=True)
    kind_of_contract = models.ForeignKey(
        ContractCore, on_delete=models.SET_NULL, null=True)
    payment_intervall = models.IntegerField(
        verbose_name= _('payment intervall'),
        null=True, blank=False
    )
    start_date = models.DateField(
        verbose_name= _('start date'),
        null=True, blank=False
    )
    end_date = models.DateField(
        verbose_name= _('end date'),
        null=True, blank=True
    )
    last_booking = models.DateField(
        verbose_name= _('last booking'),
        null=True, blank=True
    )

class Deleting_Contract(models.Model):
    member = models.ForeignKey(Member, on_delete=models.SET_NULL, null=True)
    kind_of_contract = models.IntegerField(
        verbose_name= _('kind of contract'),
        null=True, blank=False
    )
    payment_intervall = models.IntegerField(
        verbose_name= _('payment intervall'),
        null=True, blank=False
    )
    start_date = models.DateField(
        verbose_name= _('start date'),
        null=True, blank=False
    )
    end_date = models.DateField(
        verbose_name= _('end date'),
        null=True, blank=True
    )
    last_booking = models.DateField(
        verbose_name= _('last booking'),
        null=True, blank=True
    )
    edit_flag = models.CharField(
        max_length=1,
        verbose_name= _('edit flag'),
        null=True, blank=False,
    )
    edit_date = models.DateField(
        verbose_name= _('edit date'),
        auto_now_add=True,
        editable=True, blank=True
    )
