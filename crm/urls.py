from django.urls import path
# from . import views

from .views import \
    member_overview_view, \
    member_create_view, \
    member_edit_view,   \
    contract_create_view,\
    contract_edit,\
    contract_overview


urlpatterns = [
    path('member_overview', member_overview_view, name='member_overview'),
    path('member_create', member_create_view, name='member_create'),
    path('member/<int:pk>/', member_edit_view, name='member_edit'),
    path('contract_create/<int:pk>', contract_create_view, name='contract_create'),
    path('contract/<int:pk>/', contract_edit, name='contract_edit'),
    path('contract_overview', contract_overview, name='contract_overview')
    ]
