from django import forms
from .models import Member, Contract
from bootstrap_datepicker_plus import DatePickerInput
from phonenumber_field.formfields import PhoneNumberField
from django.utils.translation import gettext_lazy as _

PAYMENT=(
    ('', _('Payment Method')),
    ('1', _('direct debit')),
    ('2', _('cash'))
)

SIR_MADAM=(
    ('', _('Sir or Madam')),
    ('s', _('Sir')),
    ('m', _('Madam'))
)

PERIOD=(
    ('', _('Period of direct debit')),
    ('1', _('monthly')),
    ('40', _('quartely')),
    ('120', _('annually'))
)

CONTRACT=(
    ('', _('kind of contract')),
    ('1', _('reduced contribution')),
    ('2', _('normal contribution')),
    ('3', _('sponsor contribution')),
    ('4', _('mega sponsor contribution')),
    ('5', _('supportership')),
    ('6', _('company contribution')),
    ('7', _('special contribution'))
)

class MemberForm(forms.ModelForm):
    name                     = forms.CharField(label='',
                                               widget=forms.TextInput(attrs={'placeholder': _('Name')}))
    number                   = forms.CharField(label='',
                                               widget=forms.TextInput(attrs={'placeholder': _('Number')}))
    member_uuid              = forms.IntegerField(label='',
                                                  widget=forms.TextInput(attrs={'placeholder': _('uuid')}))
    active                   = forms.BooleanField(label='Active', required=False, initial=False)
    first_name               = forms.CharField(label='',
                                              widget=forms.TextInput(attrs={'placeholder': _('First Name')}))
    last_name                = forms.CharField(label='',
                                               widget=forms.TextInput(attrs={'placeholder': _('Last Name')}))
    salutation               = forms.CharField(label='',
                                               widget=forms.TextInput(attrs={'placeholder': _('Salutation, e.g. Dear foo')}))
    form_of_address          = forms.ChoiceField(label='', choices=SIR_MADAM)
    street                   = forms.CharField(label='',
                                               widget=forms.TextInput(attrs={'placeholder': _('Street with street number')}))
    zip                      = forms.CharField(label='',
                                               widget=forms.TextInput(attrs={'placeholder': _('ZIP')}))
    city                     = forms.CharField(label='',
                                               widget=forms.TextInput(attrs={'placeholder': _('City')}))
    country                  = forms.CharField(label='',
                                               widget=forms.TextInput(attrs={'placeholder': _('country')}))
    day_of_birth             = forms.DateField(label='',
                                               widget=DatePickerInput(format='%m/%d/%Y', attrs={'placeholder': _('day of birth')}))
    phone_number             = PhoneNumberField(label='',
                                               widget=forms.TextInput(attrs={'placeholder': _('phone number - +49...')}))
    cell_number              = PhoneNumberField(label='',
                                               widget=forms.TextInput(attrs={'placeholder': _('cell phone number - +49...')}))
    email                    = forms.EmailField(label='',
                                                widget=forms.TextInput(attrs={
                                                    'type'       : 'email',
                                                    'placeholder': _('e-mail')
                                                }))
    fax_number               = PhoneNumberField(label='',
                                               widget=forms.TextInput(attrs={'placeholder': _('telefax number - +49...')}))
    payment_method           = forms.ChoiceField(label='', choices=PAYMENT)
    iban                     = forms.CharField(label='',
                                               widget=forms.TextInput(attrs={'placeholder': _('IBAN')}))
    bic                      = forms.CharField(label='',
                                               widget=forms.TextInput(attrs={'placeholder':  _('BIC - please let it be empty if German account')}))
    name_of_bank             = forms.CharField(label='',
                                               widget=forms.TextInput(attrs={'placeholder': _('name of bank')}))
    mandate_reference        = forms.CharField(label='',
                                               widget=forms.TextInput(attrs={'placeholder':  _('mandate reference')}))
    # different_account_holder = forms.CharField(label='',
    #                                            widget=forms.TextInput(attrs={'placeholder': 'different account holder'}))
    account_holder           = forms.CharField(label='',
                                               widget=forms.TextInput(attrs={'placeholder':  _('account holder')}))
    # time_of_debit            = forms.ChoiceField(label='', choices=PERIOD)
    # kind_of_contract        = forms.ChoiceField(label='', choices=CONTRACT)
    entry_date               = forms.DateField(label='',
                                               widget=DatePickerInput(format='%m/%d/%Y', attrs={'placeholder': _('entry date')}))
    exit_date                = forms.DateField(label='',
                                               widget=DatePickerInput(format='%m/%d/%Y', attrs={'placeholder': _('exit date')}))
    memo                     = forms.CharField(label='', required=False,
                                               widget=forms.Textarea(attrs={'placeholder': _('memo field'),
                                                                             'rows' : 5,
                                                                             'cols' : 80}))


    class Meta:
        model = Member
        fields = [
            'name',
            'number',
            'member_uuid',
            'active',
            'first_name',
            'last_name',
            'salutation',
            'form_of_address',
            'street',
            'zip',
            'city',
            'country',
            'day_of_birth',
            'phone_number',
            'cell_number',
            'email',
            'fax_number',
            'payment_method',
            'iban',
            'bic',
            'name_of_bank',
            'mandate_reference',
            # 'different_account_holder',
            'account_holder',
            # 'time_of_debit',
            # 'kind_of_contract',
            'entry_date',
            'exit_date',
            'memo',
        ]
    def clean_number(self, *args, **kwargs):
        number_passed = self.cleaned_data.get("number")
        if Member.objects.filter(number=number_passed):
            raise forms.ValidationError("Double entry")
        else:
            return number_passed

class MemberEditForm(forms.ModelForm):
    name                     = forms.CharField(label='',
                                               widget=forms.TextInput(attrs={'placeholder': 'Name'}))
    number                   = forms.CharField(label='',
                                               widget=forms.TextInput(attrs={'placeholder': 'Number'}))
    member_uuid              = forms.IntegerField(label='',
                                                  widget=forms.TextInput(attrs={'placeholder': 'uuid'}))
    active                   = forms.BooleanField(label='Active', required=False, initial=False)
    first_name               = forms.CharField(label='',
                                               widget=forms.TextInput(attrs={'placeholder': 'First Name'}))
    last_name                = forms.CharField(label='',
                                               widget=forms.TextInput(attrs={'placeholder': 'Last Name'}))
    salutation               = forms.CharField(label='',
                                               widget=forms.TextInput(attrs={'placeholder': 'Salutation, e.g. Dear foo'}))
    form_of_address          = forms.ChoiceField(label='', choices=SIR_MADAM)
    street                   = forms.CharField(label='',
                                               widget=forms.TextInput(attrs={'placeholder': 'Street with street number'}))
    zip                      = forms.CharField(label='',
                                               widget=forms.TextInput(attrs={'placeholder': 'ZIP'}))
    city                     = forms.CharField(label='',
                                               widget=forms.TextInput(attrs={'placeholder': 'City'}))
    country                  = forms.CharField(label='',
                                               widget=forms.TextInput(attrs={'placeholder': 'country'}))
    day_of_birth             = forms.DateField(label='',
                                               widget=DatePickerInput(format='%m/%d/%Y', attrs={'placeholder':'day of birth'}))
    phone_number             = PhoneNumberField(label='',
                                                widget=forms.TextInput(attrs={'placeholder': 'phone number - +49...'}))
    cell_number              = PhoneNumberField(label='',
                                                widget=forms.TextInput(attrs={'placeholder': 'cell phone number - +49...'}))
    email                    = forms.EmailField(label='',
                                                widget=forms.TextInput(attrs={
                                                    'type'       : 'email',
                                                    'placeholder': 'e-mail'
                                                }))
    fax_number               = PhoneNumberField(label='',
                                                widget=forms.TextInput(attrs={'placeholder': 'telefax number - +49...'}))
    payment_method           = forms.ChoiceField(label='', choices=PAYMENT)
    iban                     = forms.CharField(label='',
                                               widget=forms.TextInput(attrs={'placeholder': 'IBAN'}))
    bic                      = forms.CharField(label='',
                                               widget=forms.TextInput(attrs={'placeholder':  'BIC - please let it be empty if German account'}))
    name_of_bank             = forms.CharField(label='',
                                               widget=forms.TextInput(attrs={'placeholder': 'name of bank'}))
    mandate_reference        = forms.CharField(label='',
                                               widget=forms.TextInput(attrs={'placeholder':  'mandate reference'}))
    # different_account_holder = forms.CharField(label='',
    #                                            widget=forms.TextInput(attrs={'placeholder': 'different account holder'}))
    account_holder           = forms.CharField(label='',
                                               widget=forms.TextInput(attrs={'placeholder':  'account holder'}))
    # time_of_debit            = forms.ChoiceField(label='', choices=PERIOD)
    # kind_of_contract        = forms.ChoiceField(label='', choices=CONTRACT)
    entry_date               = forms.DateField(label='',
                                               widget=DatePickerInput(format='%m/%d/%Y', attrs={'placeholder':'entry date'}))
    exit_date                = forms.DateField(label='',
                                               widget=DatePickerInput(format='%m/%d/%Y', attrs={'placeholder':'exit date'}))
    memo                     = forms.CharField(label='', required=False,
                                               widget=forms.Textarea(attrs={'placeholder': 'memo field',
                                                                            'rows' : 5,
                                                                            'cols' : 80}))

    class Meta:
        model = Member
        fields = [
            'name',
            'number',
            'member_uuid',
            'active',
            'first_name',
            'last_name',
            'salutation',
            'form_of_address',
            'street',
            'zip',
            'city',
            'country',
            'day_of_birth',
            'phone_number',
            'cell_number',
            'email',
            'fax_number',
            'payment_method',
            'iban',
            'bic',
            'name_of_bank',
            'mandate_reference',
            # 'different_account_holder',
            'account_holder',
            # 'time_of_debit',
            # 'kind_of_contract',
            'entry_date',
            'exit_date',
            'memo',
        ]


class ContractForm(forms.ModelForm):
    contract_kind_of_contract = forms.IntegerField(label='',
                                                   widget=forms.TextInput(attrs={'placeholder': 'kind of contract'}))
    contract_payment_intervall = forms.IntegerField(label='',
                                                    widget=forms.TextInput(attrs={'placeholder': 'payment intervall'}))
    contract_start_date       = forms.DateField(label='',
                                                widget=DatePickerInput(format='%m/%d/%Y', attrs={'placeholder':'start date'}))
    contract_end_date         = forms.DateField(label='', required=False,
                                                widget=DatePickerInput(format='%m/%d/%Y', attrs={'placeholder':'end date'}))
    class Meta:
        model=Contract
        exclude = ('member',)
        fields = [
            'contract_kind_of_contract',
            'contract_payment_intervall',
            'contract_start_date',
            'contract_end_date',
        ]

