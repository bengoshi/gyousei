��    Y      �     �      �     �     �     �     �     �     �  .   �           '     ,     <     J  J   Q  J   �     �  
   �     �      	     	     	     !	     '	     .	     C	  
   H	     S	     Z	     ^	     m	     	     �	     �	     �	     �	     �	  
   �	     �	     	
     
     
     (
     5
     >
     C
     U
     p
     u
     �
     �
     �
     �
  	   �
     �
     �
     �
     �
  	   �
     �
     �
  
   �
     
  	        %     7     Q     ]  
   b     m     r  	   z     �     �     �     �     �     �     �     �     �       
        $  
   9     D     W     e     t     �  B  �     �     �     �     �            ,        I     Q     X     i     z  e   �  \   �     F     K     S     X     [     g     v     {     �     �     �     �     �     �     �      �     �  	   �                         &     9     F  
   R  	   ]  	   g     q     u     �     �     �  
   �     �     �     �     �     �  
   �     �                    "     '     /     ;     D     T     g     v  	   ~     �  	   �     �     �     �     �     �  	   �     �     �               (     =     D     S     Z     m     |     �     �     %   R       ;              C                 
       &   ?   1   L   .   (   /   !   5   P       =                 '   6                          2           )   Y   J           D      9       Q             "   +   <                            *                  -   B   @   W   O      >            G       4   7      	          :          3          F   A   N         ,          K   E       M                  H   V   0           $   S      X      T   #   I      8   U        Account member sheet Active All Contracts All Members Are you sure? BIC BIC - please let it be empty if German account Cancel City Create Contract Create Member Delete Do you really want to delete these records? This process cannot be undone. Do you really want to delete this contract? This process cannot be undone. End date First Name IBAN ID Kind of contract Last booking Madam Member Membership number/ID Name New Member Number N° Payment Method Payment intervall Period of direct debit Salutation, e.g. Dear foo Save accounting record Sir Sir / Madan Sir or Madam Start date Street with street number ZIP address data amount credit amount debit annually cash cell phone number cell phone number - +49... city company contribution contra account contract contract id country data misc date day of birth direct debit e-mail edit flag email end date first name kind of contract last name mandate reference mega sponsor contribution member_uuid memo memo field meta monthly name data name of bank normal contribution number of member numbers payment payment intervall phone number - +49... posting text quartely reduced contribution salutation sponsor contribution start date street with number supportership telefax number telefax number - +49... uuid Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Mitgliedskonto Aktiv Alle Verträge Alle Mitglieder Bist Du sicher? BIC BIC - bei deutschen Konten bitte leer lassen Abbruch Berlin Erstelle Vertrag Mitglied anlegen Löschen Bist Du wirklich sicher, diesen Satz zu löschen? Der Prozess kann nicht rückgängig gemacht werden! Willst Du den Vertrag wirklich löschen? Der Prozess kann nicht rückgängig gemacht werden! Ende Vorname IBAN ID Vertragsart Letzte Buchung Frau Mitglied Mitgliedsnummer Name Neues Mitglied Nummer Nr. Zahlungsmethode Zahlungsintervall Intervall des Lastschrifteinzugs Anrede Speichern Herr Herr / Frau Anrede Beginn Straße mit Nummer Postleitzahl Adressdaten Habenkonto Sollkonto jährlich Bar Mobilnummer Mobilnummer - +49... Berlin Firmenmitgliedschaft Gegenkonto Vertrag Vertragsnummer Land Sonstige Daten Datum Geburtstag Lastschrift E-Mail geändert-Flag E-Mail Ende Vorname Vertragsart Nachname Mandatsreferenz Megaförderbeitrag Mitglieds_UUID Notizen Notizfeld Meta monatlich Namensdaten Name der Bank normaler Beitrag Mitgliedsnummer Nummern Zahlungen Zahlungsintervall Telefonnummer - +49... Buchungstext quartalsweise ermäßigter Beitrag Anrede Förderbeitrag Beginn Straße mit Nummer Förderbeitrag Telefaxnummer Telefaxnummer - +49... uuid 