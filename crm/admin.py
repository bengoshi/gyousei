from django.contrib import admin

# Register your models here.
from .models import Member, Deleting_Member, Contract

admin.site.register(Member)
admin.site.register(Deleting_Member)
admin.site.register(Contract)