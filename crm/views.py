from django.shortcuts import render, redirect
from django.shortcuts import get_object_or_404

from django.contrib.auth.decorators import login_required, permission_required

# Create your views here.
from .models import Member, Deleting_Member, Contract, Deleting_Contract
from django.http import HttpResponseRedirect

# from .forms import MemberForm
from .forms import MemberForm, MemberEditForm, ContractForm
from rewe.models import Journal


@permission_required("crm.view_member")
def member_overview_view(request):
    all_member = Member.objects.all()
    context = {"all_member": all_member}
    return render(request, "crm/member_overview.html", context)


@permission_required("crm.view_contract")
def contract_overview(request):
    all_contracts = Contract.objects.all()
    context = {"all_contracts": all_contracts}
    return render(request, "crm/contracts_overview.html", context)


@permission_required("crm.add_member")
def member_create_view(request):
    all_member = Member.objects.all()
    if request.method == "POST":
        form = MemberForm(request.POST or None, initial="")
        if form.is_valid():
            form.save()
            form = MemberForm(initial="")
        print(form.errors)
        context = {"all_member": all_member, "form": form}
        return render(request, "crm/member_overview.html", context)
    else:
        form = MemberForm(initial="")
        print("34: ")
    context = {"form": form}
    return render(request, "crm/member_create.html", context)


@permission_required("crm.edit_member")
def member_edit_view(request, pk):
    member = get_object_or_404(Member, pk=pk)
    member_number = int(member.number)
    initial_data = {
        "name": member.name,
        "number": member.number,
        "member_uuid": member.member_uuid,
        "active": member.active,
        "first_name": member.first_name,
        "last_name": member.last_name,
        "salutation": member.salutation,
        "form_of_address": member.form_of_address,
        "street": member.street,
        "zip": member.zip,
        "city": member.city,
        "country": member.country,
        "day_of_birth": member.day_of_birth,
        "phone_number": member.phone_number,
        "cell_number": member.cell_number,
        "email": member.email,
        "fax_number": member.fax_number,
        "payment_method": member.payment_method,
        "iban": member.iban,
        "bic": member.bic,
        "name_of_bank": member.name_of_bank,
        "mandate_reference": member.mandate_reference,
        "account_holder": member.account_holder,
        "entry_date": member.entry_date,
        "exit_date": member.exit_date,
        "memo": member.memo,
    }
    if request.method == "POST":
        form = MemberEditForm(request.POST, instance=member, initial=initial_data)
        if form.is_valid():
            if "delete_member" in request.POST:
                deleting_member = Member.objects.get(pk=member.id)
                backup_record = Deleting_Member(
                    name=deleting_member.name,
                    number=deleting_member.number,
                    member_uuid=deleting_member.member_uuid,
                    active=deleting_member.active,
                    first_name=deleting_member.first_name,
                    last_name=deleting_member.last_name,
                    salutation=deleting_member.salutation,
                    form_of_address=deleting_member.form_of_address,
                    street=deleting_member.street,
                    zip=deleting_member.zip,
                    city=deleting_member.city,
                    country=deleting_member.country,
                    day_of_birth=deleting_member.day_of_birth,
                    phone_number=deleting_member.phone_number,
                    cell_number=deleting_member.cell_number,
                    email=deleting_member.email,
                    fax_number=deleting_member.fax_number,
                    payment_method=deleting_member.payment_method,
                    iban=deleting_member.iban,
                    bic=deleting_member.bic,
                    name_of_bank=deleting_member.name_of_bank,
                    mandate_reference=deleting_member.mandate_reference,
                    account_holder=deleting_member.account_holder,
                    entry_date=deleting_member.entry_date,
                    exit_date=deleting_member.exit_date,
                    memo=deleting_member.memo,
                )
                backup_record.save()
                member.delete()
                return redirect("/member_overview")
            if "saving_modified_member" in request.POST:
                print("111")
                member = form.save(commit=False)
                member.save()
                return redirect("/member_overview")
            else:
                print("Error member_edit_view else")
        else:
            pass
            print("form is not valid, line 121")
    else:
        form = MemberForm(initial=initial_data)
    member_contract = []
    member_contract_all = []
    contracts = Contract.objects.last()
    print("contracts: ", contracts)
    try:
        last_number = contracts.id
        last_number += 1
        i = 1
        for i in range(last_number):
            try:
                contract = get_object_or_404(Contract, pk=i)
                print("contract 137: ", contracts)
                contract_member_id = contract.member.number
                print("contract_member_id 140: ", contract_member_id)
                print("member.number: ", member.number)
                if contract_member_id == member.number:
                    contract_id = contract.id
                    contract_kind_of_contract = contract.kind_of_contract
                    contract_payment_intervall = contract.payment_intervall
                    contract_start_date = contract.start_date
                    contract_end_date = contract.end_date
                    member_contract = [
                        contract_id,
                        contract_member_id,
                        contract_kind_of_contract,
                        contract_payment_intervall,
                        contract_start_date,
                        contract_end_date,
                    ]
                    member_contract_all.append(member_contract)
            except Exception as e:
                print("Exception 152: ", e)
    except Exception as e:
        print("Database is empty and Exception: ", e)
    print("finish contract: ", member_contract)
    # account sheet
    # account_number = pk
    journal = Journal.objects.last()
    last_number = journal.id
    last_number += 1
    i = 1
    account_sum = []
    account_sheet = []
    data = []
    account_record_debit_sum = 0
    account_record_credit_sum = 0
    for i in range(last_number):
        try:
            account_record = get_object_or_404(Journal, pk=i)
            account_record_debit = account_record.debit_account
            account_record_member_number = account_record.member_number
            if (account_record_debit == 2110) and (
                account_record_member_number == member_number
            ):
                data = [
                    account_record.id,
                    account_record.date_field,
                    account_record.credit_account,
                    "",
                    account_record.amount,
                    account_record.posting_text,
                ]
                account_record_debit_sum += account_record.amount
            account_record_credit = account_record.credit_account
            if (account_record_credit == 2110) and (
                account_record_member_number == member_number
            ):
                data = [
                    account_record.id,
                    account_record.date_field,
                    "",
                    account_record.debit_account,
                    account_record.amount,
                    account_record.posting_text,
                ]
                account_record_credit_sum += account_record.amount
            account_sheet.append(data)
            account_total = account_record_debit_sum - account_record_credit_sum
            account_sum = [
                account_record_debit_sum,
                account_record_credit_sum,
                account_total,
            ]
        except Exception as e:
            print("Exception in 207: ", e)
    print("before context: ", member_contract)
    context = {
        "form": form,
        "member": member,
        "member_contract": member_contract_all,
        "account_sheet": account_sheet,
        "account_sum": account_sum,
    }
    return render(request, "crm/member_edit.html", context)


@permission_required("crm.add_contract")
def contract_create_view(request, pk):
    member = get_object_or_404(Member, pk=pk)
    member_number = int(member.number)
    initial_data = {
        "name": member.name,
        "number": member.number,
        "member_uuid": member.member_uuid,
        "entry_date": member.entry_date,
        "exit_date": member.exit_date,
        "memo": member.memo,
    }
    if request.method == "POST":
        form = ContractForm(request.POST, instance=member, initial=initial_data)
        if form.is_valid():
            if "saving_modified_member" in request.POST:
                print("111")
                contract = form.save(commit=False)
                contract.save()
                return redirect("/member_overview")
            else:
                print("Error member_edit_view else")
        else:
            pass
            print("form is not valid, line 121")
    else:
        form = MemberForm(initial=initial_data)
    # member_contract
    member_contract = []
    member_contract_all = []
    contracts = Contract.objects.last()
    try:
        last_number = contracts.id
        last_number += 1
        i = 1
        for i in range(last_number):
            try:
                contract = get_object_or_404(Contract, pk=i)
                print("contract 137: ", contracts)
                contract_member_id = contract.member.number
                print("contract_member_id 140: ", contract_member_id)
                print("member.number: ", member.number)
                if contract_member_id == member.number:
                    contract_kind_of_contract = contract.kind_of_contract
                    contract_payment_intervall = contract.payment_intervall
                    contract_start_date = contract.start_date
                    contract_end_date = contract.end_date
                    member_contract = [
                        contract_member_id,
                        contract_kind_of_contract,
                        contract_payment_intervall,
                        contract_start_date,
                        contract_end_date,
                    ]
                    member_contract_all.append(member_contract)
            except Exception as e:
                print("Exception 152: ", e)
    except Exception as e:
        print("Database is empty and Exception: ", e)
    print("finish contract: ", member_contract)


@permission_required("crm.edit_contract")
def contract_edit(request, pk):
    contract = get_object_or_404(Contract, pk=pk)
    print("283 contract: ", contract)
    print("pk: ", pk)
    member_number = contract.member.number
    print("member_number: ", member_number)
    initial_data = {
        "contract_id": contract.id,
        "contract_kind_of_contract": contract.kind_of_contract,
        "contract_payment_intervall": contract.payment_intervall,
        "contract_start_date": contract.start_date,
        "contract_end_date": contract.end_date,
    }
    print("initial data: ", initial_data)
    if request.method == "POST":
        form = ContractForm(request.POST, instance=contract, initial=initial_data)
        if form.is_valid():
            if "delete_contract" in request.POST:
                deleting_contract = Contract.objects.get(pk=contract.id)
                backup_contract = Deleting_Contract(
                    contract_id=deleting_contract.id,
                    contract_kind_of_contract=deleting_contract.kind_of_contract,
                    contract_payment_intervall=deleting_contract.payment_intervall,
                    contract_start_date=deleting_contract.start_date,
                    contract_end_date=deleting_contract.end_date,
                )
                backup_contract.save()
                contract.delete()
                return redirect("/member_overview")
            if "saving_modified_contract" in request.POST:
                print("309")
                contract = form.save(commit=False)
                contract.save()
                return redirect("/member_overview")
            else:
                print("Error contract_edit_view else")
        else:
            pass
            print("form is not valid, line 121")
    else:
        form = ContractForm(initial=initial_data)
    # member_contract
    member_contract = []
    member_contract_all = []
    contracts = Contract.objects.last()
    print("contracts: ", contracts)
    try:
        last_number = contracts.id
        last_number += 1
        i = 1
        for i in range(last_number):
            try:
                contract = get_object_or_404(Contract, pk=i)
                print("contract 137: ", contracts)
                contract_member_id = contract.member.number
                print("contract_member_id 140: ", contract_member_id)
                print("member.number: ", member_number)
                if contract_member_id == member_number:
                    contract_kind_of_contract = contract.kind_of_contract
                    contract_payment_intervall = contract.payment_intervall
                    contract_start_date = contract.start_date
                    contract_end_date = contract.end_date
                    member_contract = [
                        contract_member_id,
                        contract_kind_of_contract,
                        contract_payment_intervall,
                        contract_start_date,
                        contract_end_date,
                    ]
                    member_contract_all.append(member_contract)
            except Exception as e:
                print("Exception 152: ", e)
    except Exception as e:
        print("Database is empty and Exception: ", e)
    print("finish contract: ", member_contract)
    context = {
        "form": form,
        # 'member': member,
        "member_contract": member_contract_all,
    }
    # print("context: ", context)
    return render(request, "crm/contract_edit.html", context)
