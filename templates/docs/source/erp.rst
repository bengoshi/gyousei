ERP
============
Unter der App ERP verbirgt sich der Buchhaltungsteil.


Debit position
Hier können Sollstellungen für die beim jeweiligen Mitglieder hinterlegten Verträge erstellt werden. Das System prüft dabei folgendes:
Ist das Beendigungsdatum kleiner/größer dem Sollstellungsdatum?
Ist das Startdatum kleiner/größer dem Sollstellungsdatum?
Ist das Datum der letzten Sollstellung kleiner/größer dem Sollstellungsdatum?
Die Prüfung erfolg auf Monatsebene. Wird der Austritt zum 5. Februar eingetragen, wird für den Januar noch eine Sollstellung erzeugt, nicht mehr aber für den Februar, selbst wenn die Sollstellung am 4. Februar gezogen wird.

