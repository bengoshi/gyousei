#!/usr/bin/env python
from selenium import webdriver

browser = webdriver.Firefox(executable_path=r"/usr/bin/firefox")
browser.get("http://localhost:8080/hostnet")

assert "hostnet" in browser.title
browser.quit()
