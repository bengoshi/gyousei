from django.contrib import admin
# Register your models here.
from .models import Company_data, ContractCore


admin.site.register(Company_data)
admin.site.register(ContractCore)