#!/usr/bin/env python

from django.urls import resolve
from django.test import TestCase

from core.views import landing_page

class Hostnet01BasicTest(TestCase):

    def test_url(self):
        found = resolve('/')
        self.assertEqual(found.func, landing_page)


if __name__ == '__main__':
    unittest.main()
