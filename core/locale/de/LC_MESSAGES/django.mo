��    U      �  q   l      0     1     B     I  .   M     |     �     �     �  	   �     �      �     �  	             5     :     T     \     i     v  
   {     �     �     �  	   �     �     �     �     �     	     %	     )	  "   H	     k	     r	     {	     �	     �	     �	     �	     �	  
   �	     �	     �	     
      
     -
     E
     M
     `
     o
     |
  $   �
  )   �
  +   �
               $     5     F  
   Y     d     ~  !   �     �     �     �     �     �  	              ;     I     _     r     {     �     �     �     �     �  
   �     �     �  B       S     d     k  ,   o     �     �     �     �     �     �            	   ,     6     R     W     m  
   u     �     �     �     �     �      �     �               -     D     W     o     |     �     �  	   �     �     �     �     �     �     �                    *     8     D     Z     _     o     }     �  !   �  &   �  )   �          !  	   (     2  
   >  
   I     T     j     }  	   �     �  	   �     �  $   �     �     �          "     9     J     X     m     }     �     �     �     �     �     �     :   P       L       !       U   -       C   
          O          8      ?   A   .   J                 6   D   7       2   >   K   "          0   /            I            $      5   '      #       Q   (         3   *   M   4              )   G   =       T                      <          1      S   &   H   R          B          9      +   %   @             ;   N          F   ,   E                          	           Account Overview Amount BIC BIC - please let it be empty if German account Booking Bookkeeping account for bank 1 Bookkeeping account for bank 2 City Core Data Create Event Customer-Relationship-Management Debit Position Documents Enterprise-Resource-Planning IBAN Import Accounting Records Journal Landing page Manage Event Name New Member Overview Member Payment Method Period of direct debit Reporting SEPA believer ID SEPA believer id Save accounting record Street and Number Trial Balance Sheet ZIP admission for charitable from  admission for nonprofit valid from amount annually board of management bookkeeping account call number cash cell phone number cell phone number - +49... charitable city companies'register number company contribution company name company register number country date of foundation date of ruling direct debit dispensation from corporate tax dispensation from corporate tax from dispensation from corporate tax valid for dispensation from corporate tax valid from  e-mail email internal revenue kind of contract kind of legal form legal_form liable to tax on purchase mega sponsor contribution member of the board of management monthly name of bank name of internal revenue normal contribution note for charitable from  note from number of internal revenue period of VAT phone number - +49... purpose of charter quartely reduced contribution register court special contribution sponsor contribution street supportership tax number telefax number telefax number - +49... Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Kontenübersicht Betrag BIC BIC - kann bei deutschen Konten leer bleiben Buchen Buchhaltungskonto für Bank 1 Buchhaltungskonto für Bank 2 Stadt Grund-daten Erstelle Veranstaltung Mitgliederverwaltung Sollstellungen Dokumente Enterprise-Resource-Planing IBAN Buchungen importieren Journal Startseite Veranstaltung organisieren Name Neues Mitglied Überblick Mitglieder Zahlungsmethode Intervall des Lastschrifteinzugs Berichtswesen SEPA Gläubigernummer SEPA Gläubiger-ID Buchungssatz speichern Straße und Nummer Summen- und Saldenliste Postleitzahl Gemeinnützigkeit von Gemeinnützigkeit von Betrag jährlich Vorstand Buchungskonto Telefonnummer bar Mobilnummer Mobilnummer - +49... Gemeinnützigkeit Stadt Registernummer Firmenbeitrag Firmennamen Handelsregisternummer Land Gründungsdatum Beschluss vom Lastschrift Befreiung über Körperschaft Befreiung über Körperschaft von Befreiung von der Körperschaften für Befreiung über Körperschaft gültig von E-Mail E-Mail Finanzamt Vertragsart Rechtsform Rechtsform Umsatzsteuerpflichtig Megaförderbeitrag Mitglieder des Vorstandes monatlich Bankname Finanzamt normaler Beitrag Bescheid über Gemeinnützigkeit vom Bescheid von Finanzamtsnummer USt-Voranmeldungszeitraum Telefonnummer - +49... Verwendungszweck quartalsweise ermäßigter Beitrag Vereinsregister Sonderbeitrag Förderbeitrag Straße Förderbeitrag Steuernummer Telefaxnummer Telefaxnummer - +49... 