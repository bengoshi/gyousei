from django.urls import path
from django.views.generic import TemplateView
from django.contrib.auth import views as auth_views
from django.conf.urls import include
from . import views

from core.views import company_data, landing_page, docs, DocView

urlpatterns = [
    path('company_data', company_data, name='company_data'),
    path('accounts/', include('django.contrib.auth.urls')),
    path('docs/', TemplateView.as_view(template_name="../templates/docs/html/index.html")),
    path('Doc/', views.DocView.as_view(), name='Doc'), #testing
    # path('login/', auth_views.LoginView.as_view(template_name='core/login.html'),name='login'),
    # path('login/', auth_views.LoginView.as_view(
    #     redirect_authenticated_user=True, template_name='core/login.html'), name='login'
    #      ),
    # path('login/', auth_views.LoginView.as_view(template_name='core/login.html')),
    # path('accounts/login/', auth_views.LoginView.as_view(template_name='myapp/login.html')),
    # path('docs/', docs, name='docs'),
    path('', landing_page, name='landing_page')
    # path('', Landing_page.as_view(), name='landing_page')
    ]
