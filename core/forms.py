from django import forms
from django.utils.translation import gettext_lazy as _
from .models import Company_data, ContractCore
from bootstrap_datepicker_plus import DatePickerInput
from phonenumber_field.formfields import PhoneNumberField

PAYMENT=(
    ('', _('Payment Method')),
    ('1', _('direct debit')),
    ('2', _('cash'))
)

PERIOD=(
    ('', _('Period of direct debit')),
    ('1', _('monthly')),
    ('40', _('quartely')),
    ('120', _('annually'))
)
# to do: combine it with the class Contract
CONTRACT=(
    ('', _('kind of contract')),
    ('1', _('reduced contribution')),
    ('2', _('normal contribution')),
    ('3', _('sponsor contribution')),
    ('4', _('mega sponsor contribution')),
    ('5', _('supportership')),
    ('6', _('company contribution')),
    ('7', _('special contribution'))
)

LEGAL_FORMS = (
    ('', _('kind of legal form')),
    ('1', 'eingetragener Verein (e.V.)'),
    ('2', 'gGmbH'),
    ('3', 'nicht eingetragener Verein'),
    ('4', 'Stiftung'),
    ('5', 'gUG (haftungsbeschränkt)')
)

class CompanyForm(forms.ModelForm):
    name                     = forms.CharField(label='',
                                               widget=forms.TextInput(attrs={'placeholder': _('Name')}))
    legal_form               = forms.ChoiceField(label='', choices=LEGAL_FORMS, required=False)
    street                   = forms.CharField(label='',
                                               widget=forms.TextInput(attrs={'placeholder': _('Street and Number')}))
    zip                      = forms.CharField(label='',
                                               widget=forms.TextInput(attrs={'placeholder': _('ZIP')}))
    city                     = forms.CharField(label='',
                                               widget=forms.TextInput(attrs={'placeholder': _('City')}))
    country                  = forms.CharField(label='',
                                               widget=forms.TextInput(attrs={'placeholder': _('country')}))
    phone_number             = PhoneNumberField(label='',
                                                widget=forms.TextInput(attrs={'placeholder': _('phone number - +49...')}),
                                                required=False)
    cell_number              = PhoneNumberField(label='',
                                                widget=forms.TextInput(attrs={'placeholder': _('cell phone number - +49...')}),
                                                required=False)
    email                    = forms.EmailField(label='',
                                                widget=forms.TextInput(attrs={
                                                    'type'       : 'email',
                                                    'placeholder': _('e-mail')
                                                }), required=False)
    fax_number               = PhoneNumberField(label='',
                                                widget=forms.TextInput(attrs={'placeholder': _('telefax number - +49...')}),
                                                required=False)
    sepa_believer_id         = forms.CharField(label='',
                                               widget=forms.TextInput(attrs={'placeholder': _('SEPA believer ID')}),
                                               required=False)
    iban_1                   = forms.CharField(label='',
                                               widget=forms.TextInput(attrs={'placeholder': _('IBAN')}),
                                               required=False)
    bic_1                    = forms.CharField(label='',
                                               widget=forms.TextInput(
                                                   attrs={'placeholder':  _('BIC - please let it be empty if German account')}),
                                               required=False)
    name_of_bank_1           = forms.CharField(label='',
                                               widget=forms.TextInput(attrs={'placeholder': _('name of bank')}),
                                               required=False)
    # different_account_holder = forms.CharField(label='',
    #                                            widget=forms.TextInput(attrs={'placeholder': 'different account holder'}))
    account_holder_1         = forms.CharField(label='',
                                               widget=forms.TextInput(attrs={'placeholder':  _('account holder')}),
                                               required=False)
    bookkeeping_account_1    = forms.DecimalField(label='',
                                                 widget=forms.TextInput(
                                                     attrs={'placeholder': _('Bookkeeping account for bank 1')}),
                                                  required=False)
    iban_2                   = forms.CharField(label='',
                                               widget=forms.TextInput(attrs={'placeholder': _('IBAN')}),
                                               required=False)
    bic_2                    = forms.CharField(label='',
                                               widget=forms.TextInput(
                                                   attrs={'placeholder':  _('BIC - please let it be empty if German account')}),
                                               required=False)
    name_of_bank_2           = forms.CharField(label='',
                                               widget=forms.TextInput(
                                                   attrs={'placeholder': _('name of bank')}),
                                               required=False)
    # different_account_holder = forms.CharField(label='',
    #                                            widget=forms.TextInput(attrs={'placeholder': 'different account holder'}))
    account_holder_2         = forms.CharField(label='',
                                               widget=forms.TextInput(
                                                   attrs={'placeholder':  _('account holder')}),
                                               required=False)
    bookkeeping_account_2    = forms.DecimalField(label='',
                                                 widget=forms.TextInput(
                                                     attrs={'placeholder': _('Bookkeeping account for bank 2')}),
                                                  required=False)
    internal_revenue         = forms.CharField(label='',
                                                 widget=forms.TextInput(
                                                     attrs={'placeholder': _('internal revenue')}),
                                               required=False)
    number_of_internal_revenue = forms.DecimalField(label='',
                                                    widget=forms.TextInput(
                                                        attrs={'placeholder': _('number of internal revenue')}),
                                                    required=False)
    tax_number                 = forms.CharField(label='',
                                                    widget=forms.TextInput(
                                                        attrs={'placeholder': _('tax number')}),
                                                 required=False)
    vat_number                 = forms.CharField(label='',
                                                 widget=forms.TextInput(
                                                     attrs={'placeholder': _('VAT number')}),
                                                 required=False)
    charitable                 = forms.BooleanField(label='charitable', required=False, initial=False)
    note_from                  = forms.DateField(label='',
                                                 widget=DatePickerInput(
                                                     format='%m/%d/%Y',
                                                     attrs={'placeholder': _('note for charitable from ')}),
                                                 required=False)
    admission_from             = forms.DateField(label='',
                                                 widget=DatePickerInput(format='%m/%d/%Y',
                                                                        attrs={'placeholder': _('admission for charitable from ')}),
                                                 required=False)
    dispensation_from_corporate_tax = forms.DateField(label='',
                                                      widget=DatePickerInput(
                                                          format='%m/%d/%Y',
                                                          attrs={'placeholder': _('dispensation from corporate tax')}),
                                                      required=False)
    dispensation_from_corporate_tax_valid_for = forms.DateField(label='',
                                                                widget=DatePickerInput(
                                                                    format='%m/%d/%Y',
                                                                    attrs={'placeholder': _('dispensation from corporate tax valid for')}),
                                                                required=False)
    companies_register_number  = forms.CharField(label='',
                                                widget=forms.TextInput(
                                                    attrs={'placeholder': _('company register number')}),
                                                 required=False)
    register_court             = forms.CharField(label='',
                                                 widget=forms.TextInput(
                                                     attrs={'placeholder': _('register court')}),
                                                 required=False)
    date_of_foundation         = forms.DateField(label='',
                                                 widget=DatePickerInput(
                                                     format='%m/%d/%Y', attrs={'placeholder': _('date of foundation')}),
                                                 required=False)
    purpose_of_charter         = forms.CharField(label='', required=False,
                                                 widget=forms.Textarea(attrs={'placeholder': _('purpose of charter'),
                                                                              'row': 5,
                                                                              'cols': 80}))
    liable_to_tax_on_purchases = forms.BooleanField(label='liable for VAT', required=False, initial=False)
    period_of_vat                 = forms.ChoiceField(label='', choices=PERIOD, required=False)
    board_of_management        = forms.CharField(label='', required=False,
                                                 widget=forms.Textarea(attrs={'placeholder': _('board of management'),
                                                                              'rows' : 5,
                                                                              'cols' : 80}))

    class Meta:
        model = Company_data
        fields = [
            'name',
            'legal_form',
            'street',
            'zip',
            'city',
            'country',
            'phone_number',
            'cell_number',
            'email',
            'fax_number',
            'sepa_believer_id',
            'iban_1',
            'bic_1',
            'name_of_bank_1',
            # # different_account_holder = models.BooleanField()
            'account_holder_1',
            'bookkeeping_account_1',
            'iban_2',
            'bic_2',
            'name_of_bank_2',
            # # different_account_holder = models.BooleanField()
            'account_holder_2',
            'bookkeeping_account_2',
            'internal_revenue',
            'number_of_internal_revenue',
            'tax_number',
            'vat_number',
            'charitable',
            'note_from',
            'admission_from',
            'dispensation_from_corporate_tax',
            'dispensation_from_corporate_tax_valid_for',
            'companies_register_number',
            'register_court',
            'date_of_foundation',
            'purpose_of_charter',
            'liable_to_tax_on_purchases',
            'period_of_vat',
            'board_of_management',
        ]



class ContractCoreForm(forms.ModelForm):
    name                     = forms.CharField(label='',
                                               widget=forms.TextInput(attrs={'placeholder': _('Name')}))
    amount                   = forms.DecimalField(label='',
                                                           widget=forms.TextInput(attrs={'placeholder': _('Amount')}))
    active_flag              = forms.BooleanField(label='Active Flag', required=False, initial=False)
    class Meta:
        model = ContractCore
        fields = [
            'name',
            'amount',
            'active_flag'
            ]

