from django.db import models
# from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from phonenumber_field.modelfields import PhoneNumberField
# from django.contrib.auth.models import User


class Company_data(models.Model):
    """
    Basic informations of the company.
    """
    name = models.CharField(
        max_length=120,
        verbose_name= _('company name'),
        null=True, blank=False
    )
    legal_form = models.CharField(
        max_length=120,
        verbose_name= _('legal_form'),
        null=True, blank=False
    )
    street = models.CharField(
        max_length=120,
        verbose_name= _('street'),
        null=True, blank=False
    )
    zip = models.IntegerField(
        verbose_name= _('ZIP'),
        null=True, blank=False,
    )
    city = models.CharField(
        max_length=120,
        verbose_name= _('city'),
        null=True, blank=False,
    )
    country = models.CharField(
        max_length=120,
        verbose_name= _('country'),
        null=True, blank=False,
    )
    phone_number = PhoneNumberField(
        verbose_name= _('call number'),
        null=True, blank=True,
    )
    cell_number = PhoneNumberField(
        verbose_name= _('cell phone number'),
        null=True, blank=True,
    )
    email = models.EmailField(
        max_length=120,
        verbose_name= _('email'),
        null=True, blank=False,
    )
    fax_number = PhoneNumberField(
        verbose_name= _('telefax number'),
        null=True, blank=True
    )
    sepa_believer_id = models.CharField(
        max_length=20,
        verbose_name= _('SEPA believer id'),
        null=True, blank=True
    )
    iban_1 = models.CharField(
        max_length=34,
        verbose_name= _('IBAN'),
        null=True, blank=True,
    )
    bic_1 = models.CharField(
        max_length=11,
        verbose_name= _('BIC'),
        null=True, blank=True,
    )
    name_of_bank_1 = models.CharField(
        max_length=120,
        verbose_name= _('name of bank'),
        null=True, blank=True,
    )
    # different_account_holder = models.BooleanField()
    account_holder_1 = models.CharField(
        max_length=240,
        verbose_name= _('account holder'),
        null=True, blank=True,
    )
    bookkeeping_account_1 = models.DecimalField(
        max_digits=4,
        decimal_places=0,
        verbose_name= _('bookkeeping account'),
        null=True, blank=True
    )
    iban_2 = models.CharField(
        max_length=34,
        verbose_name= _('IBAN'),
        null=True, blank=True,
    )
    bic_2 = models.CharField(
        max_length=11,
        verbose_name= _('BIC'),
        null=True, blank=True,
    )
    name_of_bank_2 = models.CharField(
        max_length=120,
        verbose_name= _('name of bank'),
        null=True, blank=True,
    )
    # different_account_holder = models.BooleanField()
    account_holder_2 = models.CharField(
        max_length=240,
        verbose_name= _('account holder'),
        null=True, blank=True,
    )
    bookkeeping_account_2 = models.DecimalField(
        max_digits=6,
        decimal_places=0,
        verbose_name= _('bookkeeping account'),
        null=True, blank=True
    )
    internal_revenue = models.CharField(
        max_length=30,
        verbose_name= _('name of internal revenue'),
        null=True, blank=True
    )
    number_of_internal_revenue = models.DecimalField(
        max_digits=4,
        decimal_places=0,
        verbose_name= _('number of internal revenue'),
        null=True, blank=True
    )
    tax_number = models.CharField(
        max_length=15,
        verbose_name= _('tax number'),
        null=True, blank=True
    )
    vat_number = models.CharField(
        max_length=30,
        verbose_name= _('vat number'),
        null=True, blank=True
    )
    charitable = models.BooleanField(
        verbose_name= _('charitable'),
        null=True, blank=True
    )
    note_from = models.DateField(
        verbose_name= _('note from'),
        null=True, blank=True
    )
    admission_from = models.DateField(
        verbose_name= _('admission for nonprofit valid from'),
        null=True, blank=True
    )
    dispensation_from_corporate_tax = models.DateField(
        verbose_name= _('dispensation from corporate tax from'),
        null=True, blank=True
    )
    dispensation_from_corporate_tax_valid_for = models.CharField(
        max_length=20,
        verbose_name= _('dispensation from corporate tax valid from '),
        null=True, blank=True
    )
    companies_register_number = models.CharField(
        max_length=30,
        verbose_name= _('companies\'register number'),
        null=True, blank=True,
    )
    register_court = models.CharField(
        max_length=60,
        verbose_name= _('register court'),
        null=True, blank=True
    )
    date_of_foundation = models.DateField(
        verbose_name= _('date of foundation'),
        null=True, blank=True
    )
    purpose_of_charter = models.TextField(
        verbose_name= _('purpose of charter'),
        null=True, blank=True
    )
    liable_to_tax_on_purchases = models.BooleanField(
        verbose_name= _('liable to tax on purchase'),
        null=True, blank=True
    )
    period_of_vat = models.CharField(
        max_length=120,
        verbose_name= _('period of VAT'),
        null=True, blank=True
    )
    board_of_management = models.TextField(
        verbose_name= _('member of the board of management'),
        null=True, blank=True
    )
    class Meta:
        app_label = 'core'
        managed = True


class ContractCore(models.Model):
    name = models.CharField(
        max_length=30,
        verbose_name= _('kind of contract'),
        null=True, blank=True,
    )
    amount = models.DecimalField(
        max_digits=100,
        decimal_places=2,
        verbose_name= _('amount'),
        null=True, blank=False,
    )
    memo = models.TextField(
        verbose_name= _('date of ruling'),
        null=True, blank=True
    )
    active_flag = models.BooleanField(null=False, blank=True)


# class Note(models.Model):
#     user = models.ForeignKey(User)
#     pub_date = models.DateTimeField()
#     title = models.CharField(max_length=200)
#     body = models.TextField()
#
#     def __unicode__(self):
#         return self.title