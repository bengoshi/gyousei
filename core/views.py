from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse
from .models import Company_data, ContractCore
from .forms import CompanyForm, ContractCoreForm
from django.views.generic import ListView, TemplateView
from django.views import View
from django.contrib.auth.decorators import login_required, permission_required

# class Landing_page(TemplateView):
#     template_name = 'core/landing_page.html'
#     def get(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#         return context


def landing_page(request):
    return render(request, "core/landing_page.html")


def docs(request):
    return render(request, "docs/_build/html/index.html")


class DocView(TemplateView):
    template_name = "docs/build/html/index.html"


@permission_required("core.change_company_data")
def company_data(request):
    company_data = Company_data.objects.last()
    if not company_data:
        if request.method == "POST":
            form = CompanyForm(request.POST or None, initial="")
            if form.is_valid():
                form.save()
                form_CompanyForm = CompanyForm(initial="")
                form_ContractCoreForm = ContractCoreForm(initial="")
            context = {"form": form_CompanyForm, "contract": form_ContractCoreForm}
            return render(request, "core/core_overview.html", context)
        else:
            form_CompanyCoreForm = CompanyForm(initial="")
            form_ContractCoreForm = ContractCoreForm(initial="")
    initial_data = {
        "name": company_data.name,
        "legal_form": company_data.legal_form,
        "street": company_data.street,
        "zip": company_data.zip,
        "city": company_data.city,
        "country": company_data.country,
        "phone_number": company_data.phone_number,
        "cell_number": company_data.cell_number,
        "email": company_data.email,
        "fax_number": company_data.fax_number,
        "sepa_believer_id": company_data.sepa_believer_id,
        "iban_1": company_data.iban_1,
        "bic_1": company_data.bic_1,
        "name_of_bank_1": company_data.name_of_bank_1,
        # # different_account_holder = models.BooleanField()
        "account_holder_1": company_data.account_holder_1,
        "bookkeeping_account_1": company_data.bookkeeping_account_1,
        "iban_2": company_data.iban_2,
        "bic_2": company_data.bic_2,
        "name_of_bank_2": company_data.name_of_bank_2,
        # # different_account_holder = models.BooleanField()
        "account_holder_2": company_data.account_holder_2,
        "bookkeeping_account_2": company_data.bookkeeping_account_2,
        "internal_revenue": company_data.internal_revenue,
        "number_of_internal_revenue": company_data.number_of_internal_revenue,
        "tax_number": company_data.tax_number,
        "vat_number": company_data.vat_number,
        "charitable": company_data.charitable,
        "note_from": company_data.note_from,
        "admission_from": company_data.admission_from,
        "dispensation_from_corporate_tax": company_data.dispensation_from_corporate_tax,
        "dispensation_from_corporate_tax_valid_for": company_data.dispensation_from_corporate_tax_valid_for,
        "companies_register_number": company_data.companies_register_number,
        "register_court": company_data.register_court,
        "date_of_foundation": company_data.date_of_foundation,
        "purpose_of_charter": company_data.purpose_of_charter,
        "liable_to_tax_on_purchases": company_data.liable_to_tax_on_purchases,
        "period_of_vat": company_data.period_of_vat,
        "board_of_management": company_data.board_of_management,
    }
    if request.method == "POST":
        form = CompanyForm(request.POST, initial=initial_data)
        if form.is_valid():
            print("valid yes")
            if "saving_company_data" in request.POST:
                form = CompanyForm(request.POST, initial=initial_data)
                form.save()
                return redirect("company_data")
        else:
            print("validation error")
    form = CompanyForm(initial=initial_data)
    context = {"form": form}
    return render(request, "core/core_overview.html", context)
