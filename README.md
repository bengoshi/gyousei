Description
==
gyousei is a software for managing membership corporations. It is designed for German GAAP particularly for a Berlin hacker space. A generalization for other membership corporations is planed. It contains a CRM for managing the club members and an ERP for the accounting. An EM for the event managing, a document management and a contract management is planed.

VERSION
==
gyousei is a raw version. So there is no beta status now. I hope to reach this at end of 2020.

Installation
==
python3.7 and pip is necessary. Suggested database is postgres. Create there a database call 'gyousei'.

Description for Linux:

* mkdir gyousei
* virtualenv -p python3 venv
* source venv/bin/activate
* git clone https://gitlab.com/bengoshi/gyousei
* cd gyousei
* pip install -r requirements.txt
* mkdir -p files/sepa_debit_position
* mkdir -p files/debit_position
* chmod u+x manage.py
* pray (for the faithful) or hope (for the other)
* ./manage makemigrations
* ./manage migrate
* ./manage runserver 8080 (8080 is the port, you can choose an other)

Go into your browser: localhost:8080

Tested with primary with Brave, but every now and then I test it on Chrome-Beta and Firefox-Dev.

Description for other OS - please complement.

Suggestions
==
Suggestions are welcome!

Manual
==
You can find the manual https://gyousei-documentation.readthedocs.io/de/latest/
And yes a lot of works is waiting...

bengoshi


