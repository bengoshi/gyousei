from django import forms
from django.utils.translation import gettext_lazy as _
from django.forms import ValidationError
from bootstrap_datepicker_plus import DatePickerInput
from .models import (
    Journal,
    Account,
    Deleting_Journal,
    Import_Account,
    Debit_position,
    Posting_key,
    Import_Posting_key,
)
from crm.models import Member
from core.models import Company_data
from rewe.views.tools import part_account_posting
from .views.UploadFileForm import UploadFileForm

MONTH = (
    ("", _("Month")),
    ("1", _("January")),
    ("2", _("February")),
    ("3", _("March")),
    ("4", _("April")),
    ("5", _("May")),
    ("6", _("June")),
    ("7", _("July")),
    ("8", _("August")),
    ("9", _("September")),
    ("10", _("October")),
    ("11", _("November")),
    ("12", _("December")),
)

YEAR = (("", _("Year")), ("2018", "2018"), ("2019", "2019"), ("2020", "2020"))


class DeletingForm(forms.ModelForm):
    class Meta:
        model = Deleting_Journal
        fields = [
            "amount",
            "debit_account",
            "credit_account",
            "date_field",
            "posting_text",
            "member_number",
            "event_number",
        ]


class BookingForm(forms.ModelForm):
    amount = forms.DecimalField(
        label="",
        localize=True,
        widget=forms.TextInput(attrs={"placeholder": _("amount")}),
    )
    posting_key = forms.IntegerField(
        label="",
        required=False,
        max_value=99,
        widget=forms.NumberInput(
            attrs={"maxlength": "3", "placeholder": _("posting key")}
        ),
    )
    debit_account = forms.IntegerField(
        label="",
        widget=forms.NumberInput(
            attrs={"maxlength": "7", "placeholder": _("debit account")}
        ),
    )
    credit_account = forms.IntegerField(
        label="",
        widget=forms.NumberInput(
            attrs={"maxlength": "5", "placeholder": _("credit account")}
        ),
    )
    date_field = forms.DateField(
        label="",
        required=True,
        widget=forms.DateInput(attrs={"placeholder": _("date")}),
    )
    posting_text = forms.CharField(
        label="",
        required=False,
        widget=forms.Textarea(
            attrs={"placeholder": _("posting text"), "rows": 1, "cols": 80}
        ),
    )
    member_number = forms.IntegerField(
        label="",
        required=False,
        widget=forms.NumberInput(attrs={"placeholder": _("member_number")}),
    )
    event_number = forms.DecimalField(
        label="",
        required=False,
        widget=forms.TextInput(attrs={"placeholder": _("event_number")}),
    )

    class Meta:
        model = Journal
        fields = [
            "amount",
            "debit_account",
            "credit_account",
            "date_field",
            "posting_text",
            "member_number",
            "event_number",
        ]

    def clean(self, *args, **kwargs):
        cleaned_data = super().clean()
        debit_account_double = cleaned_data.get("debit_account")
        credit_account_double = cleaned_data.get("credit_account")
        debit_account_passed = cleaned_data.get("debit_account")
        if debit_account_double == credit_account_double:
            msg = _("Debit account and credit account have to be unequal.")
            self.add_error("debit_account", msg)
            raise forms.ValidationError("")
        if (debit_account_double == 2110) or (credit_account_double == 2110):
            member_number = cleaned_data.get("member_number")
            member_number_passed = self.cleaned_data.get("member_number")
            if not member_number:
                msg = _("Booking on 2110 is only possible if you enter a member number")
                self.add_error("member_number", msg)
                raise forms.ValidationError("")
            elif not (Member.objects.filter(number=member_number)):
                raise forms.ValidationError(_("Member number does not exist."))
        else:
            return self.cleaned_data

    # def clean_debit_account(self, *args, **kwargs):
    #     pass

    def clean_credit_account(self, *args, **kwargs):
        credit_account_passed = self.cleaned_data.get("credit_account")
        if not (Account.objects.filter(number=credit_account_passed)):
            raise forms.ValidationError(_("Account doesn't exist."))
        else:
            return credit_account_passed

    def clean_debit_account(self, *args, **kwargs):
        debit_account_passed = self.cleaned_data.get("debit_account")
        company_data = Company_data.objects.last()
        liable_to_tax_on_purchases = company_data.liable_to_tax_on_purchases
        if liable_to_tax_on_purchases:
            debit_posting = part_account_posting(debit_account_passed)
            debit_account = debit_posting[0]
            posting_key = debit_posting[1]
            if not (Account.objects.filter(number=debit_account)):
                raise forms.ValidationError(_("Account doesn't exist."))
            elif not (Posting_key.objects.filter(tax_key=posting_key)):
                if posting_key == 0:
                    return debit_account_passed
                raise forms.ValidationError(_("Posting key doesn't exist."))
            # prüfen ob Kontorahmen MV auf E oder K oder Sammelkonto AM steht
            else:
                return debit_account_passed
        elif not (Account.objects.filter(number=debit_account_passed)):
            raise forms.ValidationError(_("Account doesn't exist."))


class AccountForm(forms.ModelForm):
    name = forms.CharField(
        label="",
        required=True,
        widget=forms.TextInput(attrs={"placeholder": "account name"}),
    )
    number = forms.DecimalField(
        label="", widget=forms.TextInput(attrs={"placeholder": "account number"})
    )
    active_flag = forms.BooleanField(label="active", required=False, initial=False)
    valid_from = forms.DateField(
        label="",
        required=False,
        widget=forms.DateInput(attrs={"placeholder": _("date")}),
    )
    summary_account = forms.CharField(
        label="",
        required=False,
        widget=forms.TextInput(attrs={"placeholder": "summary_account"}),
    )
    individual_flag = forms.BooleanField(
        label="individual", required=False, initial=False
    )
    main_function = forms.CharField(
        label="",
        required=False,
        widget=forms.TextInput(attrs={"placeholder": "main function"}),
    )
    extra_function = forms.CharField(
        label="",
        required=False,
        widget=forms.TextInput(attrs={"placeholder": "extra function"}),
    )
    function = forms.CharField(
        label="",
        required=False,
        widget=forms.TextInput(attrs={"placeholder": "function"}),
    )
    posting_key = forms.DecimalField(
        label="",
        required=False,
        widget=forms.TextInput(attrs={"placeholder": "posting key"}),
    )
    tax_rate = forms.DecimalField(
        label="",
        required=False,
        widget=forms.TextInput(attrs={"placeholder": "tax rate"}),
    )
    vat_data_sheet = forms.CharField(
        label="",
        required=False,
        widget=forms.TextInput(attrs={"placeholder": "VAT data sheet"}),
    )
    date_last_booking = forms.DateField(
        label="",
        required=False,
        widget=forms.DateInput(attrs={"placeholder": _("date")}),
    )
    kind_of_account = forms.CharField(
        label="",
        required=False,
        widget=forms.TextInput(attrs={"placeholder": "kind of account"}),
    )
    vat_line_tax_on_issue = forms.DecimalField(
        label="",
        required=False,
        widget=forms.TextInput(attrs={"placeholder": "VAT line tax on issue"}),
    )
    vat_line_tax_on_payment = forms.DecimalField(
        label="",
        required=False,
        widget=forms.TextInput(attrs={"placeholder": "VAT line tax on payment"}),
    )
    factor2_flag = forms.BooleanField(label="factor 2", required=False, initial=False)
    factor2_percentage = forms.DecimalField(
        label="",
        required=False,
        widget=forms.TextInput(attrs={"placeholder": "factor 2 %"}),
    )
    factor2_account1 = forms.DecimalField(
        label="",
        required=False,
        widget=forms.TextInput(attrs={"placeholder": "factor 2 account 1"}),
    )
    factor2_account2 = forms.DecimalField(
        label="",
        required=False,
        widget=forms.TextInput(attrs={"placeholder": "factor 2 account 2"}),
    )

    class Meta:
        model = Import_Account
        fields = [
            "name",
            "number",
            "active_flag",
            "valid_from",
            "summary_account",
            "individual_flag",
            "main_function",
            "extra_function",
            "function",
            "posting_key",
            "tax_rate",
            "vat_data_sheet",
            "date_last_booking",
            "kind_of_account",
            "vat_line_tax_on_issue",
            "vat_line_tax_on_payment",
            "factor2_flag",
            "factor2_percentage",
            "factor2_account1",
            "factor2_account2",
        ]

    def clean_name(self, *args, **kwargs):
        name_passed = self.cleaned_data.get("name")
        if Account.objects.filter(name=name_passed):
            raise forms.ValidationError("Double entry")
        else:
            return name_passed

    def clean_number(self, *args, **kwargs):
        number_passed = self.cleaned_data.get("number")
        if Account.objects.filter(number=number_passed):
            raise forms.ValidationError("Double entry")
        else:
            return number_passed


class AccountEditForm(forms.ModelForm):
    name = forms.CharField(
        label="",
        required=True,
        widget=forms.TextInput(attrs={"placeholder": "account name"}),
    )
    number = forms.DecimalField(
        label="", widget=forms.TextInput(attrs={"placeholder": "account number"})
    )
    active_flag = forms.BooleanField(label="active", required=False, initial=False)
    valid_from = forms.DateField(
        label="", required=False, widget=DatePickerInput(format="%m/%d/%Y")
    )
    summary_account = forms.CharField(
        label="",
        required=False,
        widget=forms.TextInput(attrs={"placeholder": "summary_account"}),
    )
    individual_flag = forms.BooleanField(
        label="individual", required=False, initial=False
    )
    main_function = forms.CharField(
        label="",
        required=False,
        widget=forms.TextInput(attrs={"placeholder": "main function"}),
    )
    extra_function = forms.CharField(
        label="",
        required=False,
        widget=forms.TextInput(attrs={"placeholder": "extra function"}),
    )
    function = forms.CharField(
        label="",
        required=False,
        widget=forms.TextInput(attrs={"placeholder": "function"}),
    )
    posting_key = forms.DecimalField(
        label="",
        required=False,
        widget=forms.TextInput(attrs={"placeholder": "posting key"}),
    )
    tax_rate = forms.DecimalField(
        label="",
        required=False,
        widget=forms.TextInput(attrs={"placeholder": "tax rate"}),
    )
    vat_data_sheet = forms.CharField(
        label="",
        required=False,
        widget=forms.TextInput(attrs={"placeholder": "VAT data sheet"}),
    )
    date_last_booking = forms.DateField(
        label="", required=False, widget=DatePickerInput(format="%m/%d/%Y")
    )
    kind_of_account = forms.CharField(
        label="",
        required=False,
        widget=forms.TextInput(attrs={"placeholder": "kind of account"}),
    )
    vat_line_tax_on_issue = forms.DecimalField(
        label="",
        required=False,
        widget=forms.TextInput(attrs={"placeholder": "VAT line tax on issue"}),
    )
    vat_line_tax_on_payment = forms.DecimalField(
        label="",
        required=False,
        widget=forms.TextInput(attrs={"placeholder": "VAT line tax on payment"}),
    )
    factor2_flag = forms.BooleanField(label="factor 2", required=False, initial=False)
    factor2_percentage = forms.DecimalField(
        label="",
        required=False,
        widget=forms.TextInput(attrs={"placeholder": "factor 2 %"}),
    )
    factor2_account1 = forms.DecimalField(
        label="",
        required=False,
        widget=forms.TextInput(attrs={"placeholder": "factor 2 account 1"}),
    )
    factor2_account2 = forms.DecimalField(
        label="",
        required=False,
        widget=forms.TextInput(attrs={"placeholder": "factor 2 account 2"}),
    )

    class Meta:
        model = Import_Account
        fields = [
            "name",
            "number",
            "active_flag",
            "valid_from",
            "summary_account",
            "individual_flag",
            "main_function",
            "extra_function",
            "function",
            "posting_key",
            "tax_rate",
            "vat_data_sheet",
            "date_last_booking",
            "kind_of_account",
            "vat_line_tax_on_issue",
            "vat_line_tax_on_payment",
            "factor2_flag",
            "factor2_percentage",
            "factor2_account1",
            "factor2_account2",
        ]


class AccountImportForm(forms.ModelForm):
    name = forms.CharField(
        label="", widget=forms.TextInput(attrs={"placeholder": "account name"})
    )
    number = forms.DecimalField(
        label="", widget=forms.TextInput(attrs={"placeholder": "account number"})
    )
    active_flag = forms.BooleanField(label="active", required=False, initial=False)
    valid_from = forms.DateField(
        label="", widget=forms.DateInput(attrs={"placeholder": _("date")})
    )
    summary_account = forms.CharField(
        label="", widget=forms.TextInput(attrs={"placeholder": "summary_account"})
    )
    individual_flag = forms.BooleanField(
        label="individual", required=False, initial=False
    )
    main_function = forms.CharField(
        label="", widget=forms.TextInput(attrs={"placeholder": "main function"})
    )
    extra_function = forms.CharField(
        label="", widget=forms.TextInput(attrs={"placeholder": "extra function"})
    )
    function = forms.CharField(
        label="", widget=forms.TextInput(attrs={"placeholder": "function"})
    )
    posting_key = forms.DecimalField(
        label="", widget=forms.TextInput(attrs={"placeholder": "posting key"})
    )
    tax_rate = forms.DecimalField(
        label="", widget=forms.TextInput(attrs={"placeholder": "tax rate"})
    )
    vat_data_sheet = forms.CharField(
        label="", widget=forms.TextInput(attrs={"placeholder": "VAT data sheet"})
    )
    date_last_booking = forms.DateField(
        label="", widget=forms.DateInput(attrs={"placeholder": _("date")})
    )
    kind_of_account = forms.CharField(
        label="", widget=forms.TextInput(attrs={"placeholder": "kind of account"})
    )
    vat_line_tax_on_issue = forms.DecimalField(
        label="", widget=forms.TextInput(attrs={"placeholder": "VAT line tax on issue"})
    )
    vat_line_tax_on_payment = forms.DecimalField(
        label="",
        widget=forms.TextInput(attrs={"placeholder": "VAT line tax on payment"}),
    )
    factor2_flag = forms.BooleanField(label="factor 2", required=False, initial=False)
    factor2_percentage = forms.DecimalField(
        label="", widget=forms.TextInput(attrs={"placeholder": "factor 2 %"})
    )
    factor2_account1 = forms.DecimalField(
        label="", widget=forms.TextInput(attrs={"placeholder": "factor 2 account 1"})
    )
    factor2_account2 = forms.DecimalField(
        label="", widget=forms.TextInput(attrs={"placeholder": "factor 2 account 2"})
    )

    class Meta:
        model = Import_Account
        fields = [
            "name",
            "number",
            "active_flag",
            "valid_from",
            "summary_account",
            "individual_flag",
            "main_function",
            "extra_function",
            "function",
            "posting_key",
            "tax_rate",
            "vat_data_sheet",
            "date_last_booking",
            "kind_of_account",
            "vat_line_tax_on_issue",
            "vat_line_tax_on_payment",
            "factor2_flag",
            "factor2_percentage",
            "factor2_account1",
            "factor2_account2",
        ]

    def clean_name(self, *args, **kwargs):
        name_passed = self.cleaned_data.get("name")
        if Account.objects.filter(name=name_passed):
            raise forms.ValidationError_(("Double entry"))
        else:
            return name_passed

    def clean_number(self, *args, **kwargs):
        number_passed = self.cleaned_data.get("number")
        if Account.objects.filter(number=number_passed):
            raise forms.ValidationError_(("Double entry"))
        else:
            return number_passed


# class UploadFileForm(forms.Form):
#     file = forms.FileField(widget=forms.ClearableFileInput(attrs={
#         'class:':'custom-file-input',
#         'id':'inputGroupFile01'}))
#     # file.widget.attrs.update({'class': 'form-control', 'size':20})


class DebitPositionForm(forms.Form):
    month = forms.ChoiceField(label="", choices=MONTH)
    year = forms.ChoiceField(label="", choices=YEAR)
    part_or_complete = forms.BooleanField(
        label=_("complete (if empty only for this month)"),
        required=False,
        initial=False,
    )

    class Meta:
        model = Debit_position
        fields = ["month", "year", "part_or_complete"]


class PostingKeyForm(forms.ModelForm):
    tax_key = forms.DecimalField(
        label="",
        required=True,
        widget=forms.TextInput(attrs={"placeholder": _("tax key")}),
    )
    name = forms.CharField(
        label="", widget=forms.TextInput(attrs={"placeholder": _("name")})
    )
    active_flag = forms.BooleanField(label=_("active"), required=False, initial=False)
    individual_flag = forms.BooleanField(
        label=_("individual flag"), required=False, initial=False
    )
    valid_from = forms.DateField(
        label="",
        required=False,
        widget=forms.DateInput(attrs={"placeholder": _("date")}),
    )
    tax_rate = forms.DecimalField(
        label="",
        required=False,
        widget=forms.TextInput(attrs={"placeholder": _("tax rate")}),
    )
    travel_expenses = forms.BooleanField(
        label=_("travel expenses"), required=False, initial=False
    )
    vat_account = forms.DecimalField(
        label="",
        required=False,
        widget=forms.TextInput(attrs={"placeholder": _("VAT account")}),
    )
    input_vat_account = forms.DecimalField(
        label="",
        required=False,
        widget=forms.TextInput(attrs={"placeholder": _("input VAT account")}),
    )
    vat_account_unmatured = forms.DecimalField(
        label="",
        required=False,
        widget=forms.TextInput(attrs={"placeholder": _("VAT account unmatured")}),
    )
    cash_discount_account = forms.DecimalField(
        label="",
        required=False,
        widget=forms.TextInput(attrs={"placeholder": _("cash discount account")}),
    )
    summary_account = forms.DecimalField(
        label="",
        required=False,
        widget=forms.TextInput(attrs={"placeholder": _("summary account")}),
    )
    vat_code_number_accrual_basis_tax_payer = forms.DecimalField(
        label="",
        required=False,
        widget=forms.TextInput(
            attrs={"placeholder": _("VAT code number accrual basis tax payer")}
        ),
    )
    vat_code_number_actual_basis_tax_payer = forms.DecimalField(
        label="",
        required=False,
        widget=forms.DateInput(
            attrs={"placeholder": _("VAT code number actual basis tax payer")}
        ),
    )
    factor2_flag = forms.BooleanField(
        label=_("factor2 flag"), required=True, initial=False
    )
    factor2_percentage = forms.DecimalField(
        label="",
        required=False,
        widget=forms.TextInput(attrs={"placeholder": _("factor2 percentage")}),
    )
    factor2_account1 = forms.DecimalField(
        label="",
        required=False,
        widget=forms.TextInput(attrs={"placeholder": _("factor2 account 1")}),
    )
    factor2_account2 = forms.DecimalField(
        label="",
        required=False,
        widget=forms.TextInput(attrs={"placeholder": "factor 2 account 2"}),
    )

    class Meta:
        model = Import_Account
        fields = [
            "tax_key",
            "name",
            "active_flag",
            "individual_flag",
            "valid_from",
            "tax_rate",
            "travel_expenses",
            "vat_account",
            "input_vat_account",
            "vat_account_unmatured",
            "cash_discount_account",
            "summary_account",
            "vat_code_number_accrual_basis_tax_payer",
            "vat_code_number_actual_basis_tax_payer",
            "factor2_flag",
            "factor2_percentage",
            "factor2_account1",
            "factor2_account2",
        ]

    def clean_name(self, *args, **kwargs):
        name_passed = self.cleaned_data.get("name")
        if Posting_key.objects.filter(name=name_passed):
            raise forms.ValidationError(_("Double entry"))
        else:
            return name_passed

    def clean_tax_key(self, *args, **kwargs):
        tax_key_passed = self.cleaned_data.get("tax_key")
        if Posting_key.objects.filter(number=tax_key_passed):
            raise forms.ValidationError(_("Double entry"))
        else:
            return tax_key_passed


class PostingKeyEditForm(forms.ModelForm):
    tax_key = forms.DecimalField(
        label="",
        required=True,
        widget=forms.TextInput(attrs={"placeholder": _("tax key")}),
    )
    name = forms.CharField(
        label="", widget=forms.TextInput(attrs={"placeholder": _("name")})
    )
    active_flag = forms.BooleanField(label=_("active"), required=False, initial=False)
    individual_flag = forms.BooleanField(
        label=_("individual flag"), required=False, initial=False
    )
    valid_from = forms.DateField(
        label="",
        required=False,
        widget=forms.DateInput(attrs={"placeholder": _("date")}),
    )
    tax_rate = forms.DecimalField(
        label="",
        required=False,
        widget=forms.TextInput(attrs={"placeholder": _("tax rate")}),
    )
    travel_expenses = forms.BooleanField(
        label=_("travel expenses"), required=False, initial=False
    )
    vat_account = forms.DecimalField(
        label="",
        required=False,
        widget=forms.TextInput(attrs={"placeholder": _("VAT account")}),
    )
    input_vat_account = forms.DecimalField(
        label="",
        required=False,
        widget=forms.TextInput(attrs={"placeholder": _("input VAT account")}),
    )
    vat_account_unmatured = forms.DecimalField(
        label="",
        required=False,
        widget=forms.TextInput(attrs={"placeholder": _("VAT account unmatured")}),
    )
    cash_discount_account = forms.DecimalField(
        label="",
        required=False,
        widget=forms.TextInput(attrs={"placeholder": _("cash discount account")}),
    )
    summary_account = forms.DecimalField(
        label="",
        required=False,
        widget=forms.TextInput(attrs={"placeholder": _("summary account")}),
    )
    vat_code_number_accrual_basis_tax_payer = forms.DecimalField(
        label="",
        required=False,
        widget=forms.TextInput(
            attrs={"placeholder": _("VAT code number accrual basis tax payer")}
        ),
    )
    vat_code_number_actual_basis_tax_payer = forms.DecimalField(
        label="",
        required=False,
        widget=forms.DateInput(
            attrs={"placeholder": _("VAT code number actual basis tax payer")}
        ),
    )
    factor2_flag = forms.BooleanField(
        label=_("factor2 flag"), required=True, initial=False
    )
    factor2_percentage = forms.DecimalField(
        label="",
        required=False,
        widget=forms.TextInput(attrs={"placeholder": _("factor2 percentage")}),
    )
    factor2_account1 = forms.DecimalField(
        label="",
        required=False,
        widget=forms.TextInput(attrs={"placeholder": _("factor2 account 1")}),
    )
    factor2_account2 = forms.DecimalField(
        label="",
        required=False,
        widget=forms.TextInput(attrs={"placeholder": "factor 2 account 2"}),
    )

    class Meta:
        model = Import_Account
        fields = [
            "tax_key",
            "name",
            "active_flag",
            "individual_flag",
            "valid_from",
            "tax_rate",
            "travel_expenses",
            "vat_account",
            "input_vat_account",
            "vat_account_unmatured",
            "cash_discount_account",
            "summary_account",
            "vat_code_number_accrual_basis_tax_payer",
            "vat_code_number_actual_basis_tax_payer",
            "factor2_flag",
            "factor2_percentage",
            "factor2_account1",
            "factor2_account2",
        ]
