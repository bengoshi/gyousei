from django.shortcuts import render
from django.shortcuts import get_object_or_404
from django.http import HttpResponseBadRequest, HttpResponseRedirect
from django.core.files import File
from django.conf import settings
from django.db.models import Sum

from django.utils.translation import ugettext as _

from django.contrib.auth.decorators import login_required, permission_required

from ..models import Account, \
    Journal, \
    Debit_position, \
    Debit_position_jrl, \
    Direct_debit_journal

from core.models import ContractCore, \
    Company_data
from crm.models import Member, \
    Contract
from ..forms import DebitPositionForm
from static.comparison import bigger_or_equal_than_today, \
    bigger_than_today, \
    smaller_or_equal_than_today, \
    smaller_than_today

from static.gyousei_settings import PREMIUM_ACCOUNT
from .tools import pdf_create
from sepaxml import SepaDD

from datetime import datetime
from datetime import date
import logging
import csv
logger = logging.getLogger(__name__)



@permission_required('rewe.add_journal')
def debit_position(request):
    """
    The debit_position creates debit_position from the contracts of the member.
    It will prepare for the journal and for the payment. It won't be finalizing
    anything. All will be checking through debit_position_checking before.
    Parameters:
        debit_position
    Return:
        debit_position_checking
    """
    today = datetime.now()
    # build context for showing last debit positions
    debit_position_data = Debit_position.objects.all()
    direct_debit_journal = Direct_debit_journal.objects.all()
    today_month = today.month
    today_year = today.year
    initial_data = {
        'month'           : today_month,
        'year'            : today_year,
        'part_or_complete': 'False'}
    form = DebitPositionForm(initial=initial_data)
    if request.method == 'POST':
        form = DebitPositionForm(request.POST)
        if form.is_valid():
            month_input = form.cleaned_data['month']
            year_input = form.cleaned_data['year']
            invoice_run = year_input + '-' + month_input
            # invoice_run_part is fake at this time. It is prepared for future
            invoice_run_part = form.cleaned_data[
                'part_or_complete']  # False = complete
    context = {
        'form'                : form,
        'debit_position_data' : debit_position_data,
        'direct_debit_journal': direct_debit_journal}
    last_contract = Member.objects.last()
    last_contract = last_contract.id
    last_contract += 1
    i = 1
    # print("i", i)
    if request.method == "POST" or None:
        form = DebitPositionForm(request.POST or None, initial='')
        # print("month: ", form.month, " year: ", form.year)
        if 'start_debit_position' in request.POST:
            form = DebitPositionForm()
            for i in range(last_contract):
                # check if this contract need a debit position
                debit_position_flag = False
                try:
                    contract = Contract.objects.get(pk=i)
                except Exception as e:
                    continue
                try:
                    if contract.end_date:
                        if contract.end_date is None:
                            debit_position_flag = True
                        if bigger_or_equal_than_today(contract.end_date.month,
                                                      contract.end_date.year):
                            debit_position_flag = True
                    if contract.start_date:
                        if bigger_or_equal_than_today(
                                contract.start_date.month,
                                contract.start_date.year):
                            debit_position_flag = True
                    if contract.last_booking:
                        if bigger_or_equal_than_today(
                                contract.last_booking.month,
                                contract.last_booking.year):
                            debit_position_flag = True
                except Exception as e:
                    pass
                # print("664: ", debit_position_flag)
                debit_position_flag = True  # this flag is for
                # testing so all contracts
                # would be processed
                if debit_position_flag:
                    # build booking record
                    if contract:
                        try:
                            # print("contract: ", contract)
                            kind_of_contract_member = contract.kind_of_contract
                        except Exception as e:
                            print("694 Exp: ", e)
                    try:
                        contract_core = ContractCore.objects.get(
                            pk=kind_of_contract_member.id)
                    except Exception as e:
                        print("695 Exception: ", e,
                              " |kind_of_contract_member: ",
                              kind_of_contract_member,
                              " |contract_core: ",
                              contract_core)
                    payment_flag = True
                    # if contract.member.payment_method == 1:
                    #     payment_flag = True
                    # else:
                    #     payment_flag = False
                    try:
                        booking_date = date.today()
                        # print("invoice_run: ",
                        # invoice_run, " complete?", invoice_run_part)
                        booking_record = \
                            Debit_position_jrl(amount=contract_core.amount,
                                               credit_account=contract.member.number,
                                               member_number=contract.member.number,
                                               debit_account=PREMIUM_ACCOUNT,
                                               date_field=booking_date,
                                               payed_flag=payment_flag,
                                               voucher_1=invoice_run,
                                               posting_text='debit position')
                    except Exception as e:
                        print("783 Exception: ", e)
                    try:
                        booking_record.save()
                    except Exception as e:
                        print("Saving was not possible: ", e)
        return HttpResponseRedirect('debit_position_checking')
    return render(request, "rewe/debit_position_start.html", context)


@permission_required('rewe.add_journal')
def debit_position_checking(request):
    """
    take the debit positions and
    - transfer it to the journal,
    - transfer it to the journal and generate a sepa-file
    - delete list

    Parameters:
        request from debit_position or from debit_position_checking
    Return:
        debit_position_checking.html
    """
    journal_entrys = Debit_position_jrl.objects.all()
    import_journal = Debit_position_jrl.objects.last()
    today = datetime.now()
    now = datetime.now()
    timestamp = datetime.timestamp(now)
    if request.method == "POST":
        if 'Cancel' in request.POST:
            print('cancel')
            return HttpResponseRedirect('debit_position')
        if 'delete_debit_position' in request.POST:
            deleting_record = Debit_position_jrl.objects.all()
            deleting_record.delete()
            context = dict(import_journal=journal_entrys)
            return render(request,
                          "rewe/debit_position_checking.html",
                          context
                          )
        if 'final_debit_position' or 'paying_debit_position' in request.POST:
            if 'final_debit_position' in request.POST:
                payed_flag_status = False
            if 'paying_debit_position' in request.POST:
                payed_flag_status = True
                company_data = Company_data.objects.last()
                sepa_config = {"name"       : company_data.name,
                               "IBAN"       : company_data.iban_1,
                               "BIC"        : company_data.bic_1,
                               "batch"      : True,
                               "creditor_id": company_data.sepa_believer_id,
                               "currency"   : "EUR"}
                sepa = SepaDD(sepa_config,
                              schema="pain.008.002.02",
                              clean=True)
                # collecting contracts with sepa for paying
                total_amount = 0
                accompanying_ticket = [list(sepa_config)]
                for data in Debit_position_jrl.objects.all():
                    # payed_flag = data.payed_flag
                    # print("data 816: ", data)
                    payed_flag = True  # if True there is no error
                    member_number = get_object_or_404(
                        Member, number=data.member_number)
                    member_name = member_number.account_holder
                    iban = member_number.iban
                    bic = member_number.bic
                    amount = int(data.amount * 100)
                    mandate_id = member_number.mandate_reference
                    payment_status = member_number.payment_method
                    print("payment_status: ", payment_status)
                    output = _('payment_status')
                    print(output)
                    """
                    Detect payment_method in the data of the member.
                    If 1 for direct_debit, it will collect
                    for the direct debit. Otherwise it will be skipped.
                    """
                    if payment_status == 1:
                        payed_flag = True
                        print("if: ", member_name)
                    else:
                        payed_flag = False
                        print("else: ", member_name)
                    description = (str(import_journal.date_field.year)
                                   + '-'
                                   + str(import_journal.date_field.month)
                                   + str(member_number.number))
                    if payed_flag:
                        payment = {
                            "name"           : member_name,
                            "IBAN"           : iban,
                            "BIC"            : bic,
                            "amount"         : amount,  # in cents
                            "type"           : "RCUR",  # FRST,RCUR,OOFF,FNAL
                            "collection_date": date.today(),
                            "mandate_id"     : mandate_id,
                            "mandate_date"   : date.today(),
                            "description"    : description,
                            # "endtoend_id": str(uuid.uuid1())
                            # autogenerated if obmitted
                            }
                        # print("payment: ", payment)
                        print("member_name true: ", member_name, " ", amount)
                        try:
                            sepa.add_payment(payment)
                            total_amount += amount
                            accompanying_ticket.append(payment)
                            # print("payment 848: ", accompanying_ticket)
                        except Exception as e:
                            print("Ex: ", e)
                    if not payed_flag:
                        print("member_name not: ", member_name, " ", amount)
                    # sepa.export will have to be tested
                    # and the exception will have to shown for user
                    # print("Sepa-Export", sepa.export(validate=True))
                id_sepa_file = Debit_position.objects.last().id
                id_sepa_file += 1
                sepa_filename = (settings.BASE_DIR
                                 + settings.MEDIA_URL
                                 + 'sepa_debit_position/SEPA-'
                                 + str(id_sepa_file))
                with open(sepa_filename + '.xml', 'w') as f:
                    myfile = File(f)
                    sepa_export = sepa.export(validate=True)
                    sepa_export = sepa_export.decode()
                    myfile.write(sepa_export)
                    myfile.closed
                    f.closed
                sepa_filename_short = 'SEPA-' + str(id_sepa_file)
                pdf_create(str(sepa_filename),
                           str(accompanying_ticket),
                           'sepa_debit_position',
                           sepa_filename_short)
                total_amount_100 = total_amount / 100
                update_direct_debit = Direct_debit_journal(
                    month=import_journal.date_field.month,
                    year=import_journal.date_field.year,
                    creation_date=today,
                    origin=True,
                    sum_of_debit_positions=total_amount_100,
                    direct_debit_file_pdf='/sepa_debit_position/' + str(sepa_filename_short) + '.pdf',
                    sepa_file='/sepa_debit_position/' + str(sepa_filename_short) + '.xml',
                    uploaded_at=today
                    )
                update_direct_debit.save()
            journal_entrys_printing = []
            try:
                total_amount = Debit_position_jrl.objects.aggregate(
                    Sum('amount'))
                total_amount = total_amount['amount__sum']
                if total_amount is None:
                    total_amount = '0'
            except Exception as e:
                print("total amount: ", e, " 896")
            for account_record in Debit_position_jrl.objects.all():
                try:
                    final_journal = Journal(
                        amount=account_record.amount,
                        credit_account=account_record.credit_account,
                        member_number=account_record.member_number,
                        debit_account=account_record.debit_account,
                        date_field=account_record.date_field,
                        voucher_1=account_record.voucher_1,
                        posting_text=account_record.posting_text,
                        payed_flag=payed_flag_status)
                    # total_amount += account_record.amount
                except Exception as e:
                    print("717: ", e)
                try:
                    final_journal.save()
                    journal_entrys_printing.append(final_journal.amount)
                    journal_entrys_printing.append(
                        final_journal.credit_account)
                    journal_entrys_printing.append(final_journal.member_number)
                    journal_entrys_printing.append(final_journal.debit_account)
                    journal_entrys_printing.append(final_journal.date_field)
                    journal_entrys_printing.append(final_journal.voucher_1)
                    journal_entrys_printing.append(final_journal.posting_text)
                    journal_entrys_printing.append(final_journal.payed_flag)
                    account_record.delete()
                except Exception as e:
                    print("Exception 724: ", e)
            filename = "debit-position-" + str(timestamp)
            update_debit_position = Debit_position(
                month=import_journal.date_field.month,
                year=import_journal.date_field.year,
                creation_date=today,
                part_or_complete=True,
                sum_of_debit_positions=total_amount,
                debit_position_file_pdf='/debit_position/'
                                        + str(filename)
                                        + '.pdf',
                debit_position_file_csv = '/debit_position/'
                                          + str(filename)
                                          + '.csv')
            filename_csv = (settings.BASE_DIR
                            + settings.MEDIA_URL
                            + '/debit_position/'
                            + filename
                            + '.csv')
            with open(filename_csv, 'w') as csvfile:
                writer = csv.writer(csvfile, delimiter=';')
                writer.writerow(journal_entrys_printing)
            pdf_create(str(filename),
                       str(journal_entrys_printing),
                       'debit_position',
                       filename)
            update_debit_position.save()
            # context = dict(import_journal=journal_entrys)
            return HttpResponseRedirect('debit_position')
        else:
            return HttpResponseBadRequest()
    else:
        context = dict(import_journal=journal_entrys)
        return render(request,
                      "rewe/debit_position_checking.html",
                      context)
