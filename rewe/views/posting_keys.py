import logging

from django.contrib.auth.decorators import permission_required
from django.http import HttpResponseBadRequest
from django.shortcuts import get_object_or_404
from django.shortcuts import redirect
from django.shortcuts import render

from ..models import Posting_key, Import_Posting_key, Deleting_Posting_key

from ..forms import UploadFileForm, PostingKeyForm, PostingKeyEditForm


# from django.utils.translation import ugettext as _
logger = logging.getLogger(__name__)


@permission_required("rewe.view_account")
def posting_key_overview(request):
    sorted_posting_key = Posting_key.objects.order_by("tax_key")
    context = dict(posting_key=sorted_posting_key)
    return render(request, "rewe/posting_key_overview.html", context)


@permission_required("rewe.add_account")
def posting_key_import(request):
    if request.method == "POST":
        form = UploadFileForm(request.POST, request.FILES)

        def posting_key_func(row):
            print("func-row: ", row)
            k = Import_Posting_key.objects.filter(slug=row[0])[0]
            row[0] = k
            print("func-row-below: ", row)
            return row

        if form.is_valid():
            print("valid!")
            try:
                request.FILES["file"].save_book_to_database(
                    models=[Import_Posting_key],
                    initializers=[None, posting_key_func],
                    mapdicts=[
                        [
                            "tax_key",
                            "name",
                            "active_flag",
                            "valid_from",
                            "individual_flag",
                            "tax_rate",
                            "vat_account",
                            "input_vat_account",
                            "vat_account_unmatured",
                            "cash_discount_account",
                            "factor2_flag",
                            "factor2_percentage",
                            "factor2_account1",
                            "factor2_account2",
                            "summary_account",
                            "vat_code_number_accrual_basis_tax_payer",
                            "vat_code_number_actual_basis_tax_payer",
                        ]
                    ],
                )
            except Exception as e:
                print("66")
                print("Exception in mapdicts: ", e)
                print("file: ", form)
                print("content: ", request.FILES["file"])
            return redirect("posting_key_import_checking")
        else:
            return HttpResponseBadRequest()
    else:
        form = UploadFileForm()
    return render(
        request,
        "rewe/posting_key_import.html",
        {
            "form": form,
            "title": "Import excel data into database example",
            "header": "Please upload sample-data.xls:",
        },
    )


@permission_required("rewe.add_account")
def posting_key_import_checking(request):
    posting_key_entrys = Import_Posting_key.objects.all()
    if request.method == "POST" or None:
        if "delete_import_posting_key" in request.POST:
            deleting_posting_key = Import_Posting_key.objects.all()
            deleting_posting_key.delete()
            context = dict(import_posting_key=posting_key_entrys)
            return redirect("posting_key_import_checking")
        if "final_import_posting_key" in request.POST:
            print("426")
            for posting_key_data in Import_Posting_key.objects.all():
                try:  # in case of not consecutively numbered id
                    # posting_key = Import_Posting_key.objects.get(pk=i)
                    # print("account: ", account)
                    print("yip", posting_key_data.tax_key)
                    final_posting_key = Posting_key(
                        tax_key=posting_key_data.tax_key,
                        name=posting_key_data.name,
                        active_flag=posting_key_data.active_flag,
                        individual_flag=posting_key_data.individual_flag,
                        valid_from=posting_key_data.valid_from,
                        tax_rate=posting_key_data.tax_rate,
                        vat_account=posting_key_data.vat_account,
                        input_vat_account=posting_key_data.input_vat_account,
                        vat_account_unmatured=posting_key_data.vat_account_unmatured,
                        cash_discount_account=posting_key_data.cash_discount_account,
                        summary_account=posting_key_data.summary_account,
                        vat_code_number_accrual_basis_tax_payer=posting_key_data.vat_code_number_accrual_basis_tax_payer,
                        vat_code_number_actual_basis_tax_payer=posting_key_data.vat_code_number_actual_basis_tax_payer,
                        factor2_flag=posting_key_data.factor2_flag,
                        factor2_percentage=posting_key_data.factor2_percentage,
                        factor2_account1=posting_key_data.factor2_account1,
                        factor2_account2=posting_key_data.factor2_account2,
                    )
                    print("fine")
                    try:
                        final_posting_key.save()
                    except Exception as e:
                        print("Exception save: ", e)
                    posting_key_data.delete()
                except Exception as e:
                    print("Exception 128: ", e)
            context = dict(import_posting_key=posting_key_entrys)
            return render(request, "rewe/posting_key_import_checking.html", context)
        else:
            print("Error 134 posting_key_import_checking")
            return HttpResponseBadRequest()
    else:
        context = dict(import_posting_key=posting_key_entrys)
        return render(request, "rewe/posting_key_import_checking.html", context)


@permission_required("rewe.add_account")
def posting_key_create(request):
    posting_key_entrys = Posting_key.objects.all()
    if request.method == "POST":
        form = PostingKeyForm(request.POST or None, initial="")
        if form.is_valid():
            form.save()
            form = PostingKeyForm(initial="")
        context = {"posting_key": posting_key_entrys, "form": form}
        return render(request, "rewe/posting_key_create.html", context)
    else:
        form = PostingKeyForm(initial="")
    context = {"posting_key": posting_key_entrys, "form": form}
    return render(request, "rewe/posting_key_create.html", context)


@permission_required("rewe.edit_account")
def posting_key_edit(request, pk):
    posting_key = get_object_or_404(Posting_key, pk=pk)
    print("posting_key: ", posting_key)
    initial_data = dict(
        tax_key=posting_key.tax_key,
        name=posting_key.name,
        active_flag=posting_key.active_flag,
        individual_flag=posting_key.individual_flag,
        valid_from=posting_key.valid_from,
        tax_rate=posting_key.tax_rate,
        travel_expenses=posting_key.travel_expenses,
        vat_account=posting_key.vat_account,
        input_vat_account=posting_key.input_vat_account,
        vat_account_unmatured=posting_key.vat_account_unmatured,
        cash_discount_account=posting_key.cash_discount_account,
        summary_account=posting_key.summary_account,
        vat_code_number_accrual_basis_tax_payer=posting_key.vat_code_number_accrual_basis_tax_payer,
        vat_code_number_actual_basis_tax_payer=posting_key.vat_code_number_actual_basis_tax_payer,
        factor2_flag=posting_key.factor2_flag,
        factor2_percentage=posting_key.factor2_percentage,
        factor2_account1=posting_key.factor2_account1,
        factor2_account2=posting_key.factor2_account2,
    )
    if request.method == "POST":
        form = PostingKeyEditForm(
            request.POST, instance=posting_key, initial=initial_data
        )
        if "delete_posting_key" in request.POST:
            print("delete posting_key: ", request.POST)
            deleting_posting_key = Posting_key.objects.get(pk=account.id)
            backup_posting_key = Deleting_Posting_key(
                tax_key=deleting_posting_key.tax_key,
                name=deleting_posting_key.name,
                active_flag=deleting_posting_key.active_flag,
                individual_flag=deleting_posting_key.individual_flag,
                valid_from=deleting_posting_key.valid_from,
                tax_rate=deleting_posting_key.tax_rate,
                travel_expenses=deleting_posting_key.travel_expenses,
                vat_account=deleting_posting_key.vat_account,
                input_vat_account=deleting_posting_key.input_vat_account,
                vat_account_unmatured=deleting_posting_key.vat_account_unmatured,
                cash_discount_account=deleting_posting_key.cash_discount_account,
                summary_account=deleting_posting_key.summary_account,
                vat_code_number_accrual_basis_tax_payer=deleting_posting_key.vat_code_number_accrual_basis_tax_payer,
                vat_code_number_actual_basis_tax_payer=deleting_posting_key.vat_code_number_actual_basis_tax_payer,
                factor2_flag=deleting_posting_key.factor2_flag,
                factor2_percentage=deleting_posting_key.factor2_percentage,
                factor2_account1=deleting_posting_key.factor2_account1,
                factor2_account2=deleting_posting_key.factor2_account2,
                edit_flag="d",
            )
            backup_posting_key.save()
            posting_key.delete()
            return redirect("posting_key_overview")
        if form.is_valid():
            if "saving_modified_posting_key" in request.POST:
                deleting_posting_key = Posting_key.objects.get(pk=posting_key.id)
                backup_posting_key = Deleting_Posting_key(
                    tax_key=deleting_posting_key.tax_key,
                    name=deleting_posting_key.name,
                    active_flag=deleting_posting_key.active_flag,
                    individual_flag=deleting_posting_key.individual_flag,
                    valid_from=deleting_posting_key.valid_from,
                    tax_rate=deleting_posting_key.tax_rate,
                    travel_expenses=deleting_posting_key.travel_expenses,
                    vat_account=deleting_posting_key.vat_account,
                    input_vat_account=deleting_posting_key.input_vat_account,
                    vat_account_unmatured=deleting_posting_key.vat_account_unmatured,
                    cash_discount_account=deleting_posting_key.cash_discount_account,
                    summary_account=deleting_posting_key.summary_account,
                    vat_code_number_accrual_basis_tax_payer=deleting_posting_key.vat_code_number_accrual_basis_tax_payer,
                    vat_code_number_actual_basis_tax_payer=deleting_posting_key.vat_code_number_actual_basis_tax_payer,
                    factor2_flag=deleting_posting_key.factor2_flag,
                    factor2_percentage=deleting_posting_key.factor2_percentage,
                    factor2_account1=deleting_posting_key.factor2_account1,
                    factor2_account2=deleting_posting_key.factor2_account2,
                    edit_flag="e",
                )
                backup_posting_key.save()
                posting_key_now = form.save(commit=False)
                posting_key_now.save()
                return redirect("posting_key_edit", pk)
            else:
                print("Error posting_key edit else")
    else:
        form = PostingKeyEditForm(initial=initial_data)
    context = {"form": form, "posting_key": posting_key}
    return render(request, "rewe/posting_key_edit.html", context)
