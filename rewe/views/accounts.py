from django.shortcuts import render
from django.shortcuts import get_object_or_404
from django.shortcuts import redirect
from django.http import HttpResponseBadRequest

from django.utils.translation import ugettext as _

from django.contrib.auth.decorators import login_required, permission_required

from ..models import Account, \
    Deleting_Account, \
    Import_Account

from ..forms import BookingForm, \
    AccountForm, \
    AccountEditForm

from .tools import UploadFileForm

import logging
logger = logging.getLogger(__name__)



@permission_required('rewe.view_account')
def account_overview(request):
    sorted_accounts = Account.objects.order_by('number')
    context = dict(account_object=sorted_accounts)
    return render(request, "rewe/accounts_overview.html", context)



@permission_required('rewe.add_account')
def account_create(request):
    account_entrys = Account.objects.all()
    if request.method == "POST":
        form = AccountForm(request.POST or None, initial='')
        if form.is_valid():
            form.save()
            form = AccountForm(initial='')
        context = {
            'account': account_entrys,
            'form'   : form
            }
        return render(request, "rewe/accounts_create.html", context)
    else:
        form = AccountForm(initial='')
    context = {
        'account': account_entrys,
        'form'   : form
        }
    return render(request, "rewe/accounts_create.html", context)


@permission_required('rewe.edit_account')
def account_edit(request, pk):
    account = get_object_or_404(Account, pk=pk)
    # print("account: ", account)
    initial_data = {
        'number'                 : account.number,
        'name'                   : account.name,
        'active_flag'            : account.active_flag,
        'valid_from'             : account.valid_from,
        'summary_account'        : account.summary_account,
        'individual_flag'        : account.individual_flag,
        'main_function'          : account.main_function,
        'extra_function'         : account.extra_function,
        'function'               : account.function,
        'posting_key'            : account.posting_key,
        'tax_rate'               : account.tax_rate,
        'vat_data_sheet'         : account.vat_data_sheet,
        'date_last_booking'      : account.date_last_booking,
        'kind_of_account'        : account.kind_of_account,
        'vat_line_tax_on_issue'  : account.vat_line_tax_on_issue,
        'vat_line_tax_on_payment': account.vat_line_tax_on_payment,
        'factor2_flag'           : account.factor2_flag,
        'factor2_percentage'     : account.factor2_percentage,
        'factor2_account1'       : account.factor2_account1,
        'factor2_account2'       : account.factor2_account2
        }
    if request.method == "POST":
        form = AccountEditForm(request.POST,
                               instance=account,
                               initial=initial_data)
        if 'delete_account' in request.POST:
            deleting_account = Account.objects.get(pk=account.id)
            backup_account = Deleting_Account(
                number=deleting_account.number,
                name=deleting_account.name,
                active_flag=deleting_account.active_flag,
                valid_from=deleting_account.valid_from,
                summary_account=deleting_account.summary_account,
                individual_flag=deleting_account.individual_flag,
                main_function=deleting_account.main_function,
                extra_function=deleting_account.extra_function,
                function=deleting_account.function,
                posting_key=deleting_account.posting_key,
                tax_rate=deleting_account.tax_rate,
                vat_data_sheet=deleting_account.vat_data_sheet,
                date_last_booking=deleting_account.date_last_booking,
                kind_of_account=deleting_account.kind_of_account,
                vat_line_tax_on_issue=deleting_account.vat_line_tax_on_issue,
                vat_line_tax_on_payment=deleting_account.vat_line_tax_on_payment,
                factor2_flag=deleting_account.factor2_flag,
                factor2_percentage=deleting_account.factor2_percentage,
                factor2_account1=deleting_account.factor2_account1,
                factor2_account2=deleting_account.factor2_account2,
                edit_flag='d'
                )
            backup_account.save()
            account.delete()
            return redirect('account_overview')
        if form.is_valid():
            if 'saving_modified_account' in request.POST:
                deleting_account = Account.objects.get(pk=account.id)
                backup_account = Deleting_Account(
                    number=deleting_account.number,
                    name=deleting_account.name,
                    active_flag=deleting_account.active_flag,
                    valid_from=deleting_account.valid_from,
                    summary_account=deleting_account.summary_account,
                    individual_flag=deleting_account.individual_flag,
                    main_function=deleting_account.main_function,
                    extra_function=deleting_account.extra_function,
                    function=deleting_account.function,
                    posting_key=deleting_account.posting_key,
                    tax_rate=deleting_account.tax_rate,
                    vat_data_sheet=deleting_account.vat_data_sheet,
                    date_last_booking=deleting_account.date_last_booking,
                    kind_of_account=deleting_account.kind_of_account,
                    vat_line_tax_on_issue=deleting_account.vat_line_tax_on_issue,
                    vat_line_tax_on_payment=deleting_account.vat_line_tax_on_payment,
                    factor2_flag=deleting_account.factor2_flag,
                    factor2_percentage=deleting_account.factor2_percentage,
                    factor2_account1=deleting_account.factor2_account1,
                    factor2_account2=deleting_account.factor2_account2,
                    edit_flag='e'
                    )
                backup_account.save()
                account_account = form.save(commit=False)
                account_account.save()
                return redirect('account_edit', pk)
            else:
                print("Error account_edit else")
    else:
        form = AccountEditForm(initial=initial_data)
    context = {
        'form'   : form,
        'account': account
        }
    return render(request, 'rewe/accounts_edit.html', context)



@permission_required('rewe.add_account')
def import_accounts(request):
    if request.method == "POST":
        form = UploadFileForm(request.POST,
                              request.FILES)
        def account_func(row):
            k = Import_Account.objects.filter(slug=row[0])[0]
            row[0] = k
            return row
        if form.is_valid():
            try:
                request.FILES['file'].save_book_to_database(
                    models=[Import_Account],
                    initializers=[None, account_func],
                    mapdicts=[
                        ['number',
                         'name',
                         'active_flag',
                         'valid_from',
                         'summary_account',
                         'individual_flag',
                         'main_function',
                         'extra_function',
                         'function',
                         'posting_key',
                         'tax_rate',
                         'vat_data_sheet',
                         'date_last_booking',
                         'kind_of_account',
                         'vat_line_tax_on_issue',
                         'vat_line_tax_on_payment',
                         'factor2_flag',
                         'factor2_percentage',
                         'factor2_account1',
                         'factor2_account2',
                         ]]
                    )
            except Exception as e:
                print("import accounts - mapdicts: ", e)
            return redirect('import_account_checking')
        else:
            return HttpResponseBadRequest()
    else:
        form = UploadFileForm()
    return render(
        request,
        'rewe/accounts_import_form.html',
        {
            'form'  : form,
            'title' : 'gyousei import accounts',
            'header': 'Please upload accounts.xls:'
            })


@permission_required('rewe.add_account')
def import_account_checking(request):
    account_entrys = Import_Account.objects.all()
    if request.method == "POST" or None:
        if 'delete_import_account' in request.POST:
            deleting_account = Import_Account.objects.all()
            deleting_account.delete()
            context = dict(import_account=account_entrys)
            return render(request,
                          "rewe/accounts_import_checking.html",
                          context)
        if 'final_import_account' in request.POST:
            for i in range(len(Import_Account.objects.all())):
                try:  # in case of not consecutively numbered id
                    account = Import_Account.objects.get(pk=i)
                    final_account = Account(
                        number=account.number,
                        name=account.name,
                        active_flag=account.active_flag,
                        summary_account=account.summary_account,
                        individual_flag=account.individual_flag,
                        main_function=account.main_function,
                        extra_function=account.extra_function,
                        function=account.function,
                        posting_key=account.posting_key,
                        tax_rate=account.tax_rate,
                        vat_data_sheet=account.vat_data_sheet,
                        date_last_booking=account.date_last_booking,
                        kind_of_account=account.kind_of_account,
                        vat_line_tax_on_issue=account.vat_line_tax_on_issue,
                        vat_line_tax_on_payment=account.vat_line_tax_on_payment,
                        factor2_flag=account.factor2_flag,
                        factor2_percentage=account.factor2_percentage,
                        factor2_account1=account.factor2_account1,
                        factor2_account2=account.factor2_account2)
                    try:
                        final_account.save()
                    except Exception as e:
                        print("Exception: ", e)
                    account.delete()
                except:
                    pass
            context = {
                'import_account': account_entrys
                }
            return render(request,
                          "rewe/accounts_import_checking.html",
                          context)
        else:
            print("Error import_account_checking")
            return HttpResponseBadRequest()
    else:
        context = {
            'import_account': account_entrys
            }
        return render(request,
                      "rewe/accounts_import_checking.html",
                      context)

