from django import forms

class UploadFileForm(forms.Form):
    file = forms.FileField(widget=forms.ClearableFileInput(attrs={
        'class:':'custom-file-input',
        'id':'inputGroupFile01'}))
    # file.widget.attrs.update({'class': 'form-control', 'size':20})
