from django.shortcuts import render
from django.shortcuts import get_object_or_404
from django.shortcuts import redirect
from django.http import HttpResponseBadRequest

from django.utils.translation import ugettext as _

from django.contrib.auth.decorators import login_required, permission_required

from ..models import (
    Account,
    Journal,
    Import_Journal,
    Deleting_Journal,
    Posting_key,
    Import_Posting_key,
    Deleting_Posting_key,
)
from core.models import Company_data
from ..forms import BookingForm, UploadFileForm

import logging
import decimal
from .tools import part_account_posting, tax_calc_posting_key

logger = logging.getLogger(__name__)


@permission_required("rewe.add_journal")
def journal_booking(request):
    journal_entrys = Journal.objects.all()
    if request.method == "POST":
        form = BookingForm(request.POST or None, initial="")
        if form.is_valid():
            formular = form.save(commit=False)
            debit_posting = part_account_posting(formular.debit_account)
            debit_account = debit_posting[0]
            posting_key = debit_posting[1]
            if posting_key is None or 0:
                gross_amount = formular.amount
                tax_amount = 0
                net_amount = gross_amount
            if posting_key is not None:
                gross_amount = formular.amount
                tax = tax_calc_posting_key(gross_amount, posting_key)
                net_amount = tax[0]
                tax_amount = tax[1]
            if (Account.objects.filter(number=debit_account)) == "AM":
                print("AM: ")
                # hier muss noch der Fall hin, dass Sammelkonto AM ist
                pass
            formular.debit_account = debit_account
            formular.posting_key = posting_key
            formular.net_amount = net_amount
            formular.tax_amount = tax_amount
            form.save()
            form = BookingForm(initial="")
        context = {"journal": journal_entrys, "form": form}
        return render(request, "rewe/booking_journal.html", context)
    else:
        form = BookingForm(initial="")
    context = {"journal": journal_entrys, "form": form}
    return render(request, "rewe/booking_journal.html", context)


@permission_required("rewe.edit_account")
def accounting_record_edit(request, pk):
    account_record = get_object_or_404(Journal, pk=pk)
    initial_data = {
        "amount": account_record.amount,
        "posting_key": account_record.posting_key,
        "debit_account": account_record.debit_account,
        "credit_account": account_record.credit_account,
        "date_field": account_record.date_field,
        "posting_text": account_record.posting_text,
        "member_number": account_record.member_number,
        "event_number": account_record.event_number,
    }
    if request.method == "POST":
        form = BookingForm(request.POST, instance=account_record, initial=initial_data)
        if "delete_accounting_record" in request.POST:
            deleting_record = Journal.objects.get(pk=account_record.id)
            backup_record = Deleting_Journal(
                amount=deleting_record.amount,
                posting_key=deleting_record.posting_key,
                debit_account=deleting_record.debit_account,
                credit_account=deleting_record.credit_account,
                date_field=deleting_record.date_field,
                posting_text=deleting_record.posting_text,
                edit_flag="d",
            )
            backup_record.save()
            account_record.delete()
            return redirect("/")
        if form.is_valid():
            if "saving_modified_accounting_record" in request.POST:
                deleting_record = Journal.objects.get(pk=account_record.id)
                backup_record = Deleting_Journal(
                    amount=deleting_record.amount,
                    posting_key=deleting_record.posting_key,
                    debit_account=deleting_record.debit_account,
                    credit_account=deleting_record.credit_account,
                    date_field=deleting_record.date_field,
                    posting_text=deleting_record.posting_text,
                    edit_flag="e",
                )
                backup_record.save()
                account_record = form.save(commit=False)
                account_record.save()
                return redirect("/")
            else:
                print("Error accounting_record_edit else")
    else:
        form = BookingForm(initial=initial_data)
    context = {"form": form, "account_record": account_record}
    return render(request, "rewe/booking_accounting_record_edit.html", context)


@permission_required("rewe.add_journal")
def import_data(request):
    if request.method == "POST":
        form = UploadFileForm(request.POST, request.FILES)

        def journal_func(row):
            j = Import_Journal.objects.filter(slug=row[0])[0]
            row[0] = j
            return row

        if form.is_valid():
            request.FILES["file"].save_book_to_database(
                models=[Import_Journal],
                initializers=[None, journal_func],
                mapdicts=[
                    [
                        "amount",
                        "posting_key",
                        "debit_account",
                        "credit_account",
                        "date_field",
                        "posting_text",
                    ]
                ],
            )
            return redirect("import_journal_checking")
        else:
            return HttpResponseBadRequest()
    else:
        form = UploadFileForm()
    return render(
        request,
        "rewe/booking_import_form.html",
        {
            "form": form,
            "title": "Import excel data into gyousei",
            "header": "Please upload account-record-data.xls:",
        },
    )


@permission_required("rewe.add_journal")
def import_datev(request):
    if request.method == "POST":
        form = UploadFileForm(request.POST, request.FILES)

        def journal_func(row):
            j = Import_Journal.objects.filter(slug=row[0])[0]
            print("jf: ", j)
            row[0] = j
            print("\n r: ", row)
            return row

        if form.is_valid():
            print("\n form: ", form)
            print("\n request: ", request.FILES["file"])
            print("\n request: ", request.POST)
            request.FILES["file"].save_book_to_database(
                models=[Import_Journal],
                initializers=[None, journal_func],
                mapdicts=[
                    [
                        "amount",
                        "debit_flag",
                        "debit_account",
                        "credit_account",
                        "posting_key",
                        "voucher_2",  # date_field
                        "voucher_1",
                        "posting_text",
                    ]
                ],
            )
            print("redirect")
            return redirect("import_journal_checking")
        else:
            return HttpResponseBadRequest()
    else:
        form = UploadFileForm()
    return render(
        request,
        "rewe/booking_import_form.html",
        {
            "form": form,
            "title": "Import excel data into gyousei",
            "header": "Please upload account-record-data.xls:",
        },
    )


@permission_required("rewe.add_journal")
def import_journal_checking(request):
    journal_entrys = Import_Journal.objects.all()
    for account_record in journal_entrys:
        error_number = []
        if not Account.objects.filter(number=account_record.debit_account):
            error_number.append(1)
        if not Account.objects.filter(number=account_record.credit_account):
            error_number.append(2)
        if not Posting_key.objects.filter(tax_key=account_record.posting_key):
            error_number.append(3)
        if account_record.debit_flag != None or account_record.debit_flag != "":
            error_number.append(4)
        if account_record.date_field == "None" or account_record.date_field == "":
            error_number.append(5)
        try:
            account_record.error_number = error_number  # hier kommt der Konflixt her
        except Exception as e:
            print("227: ", e)
        try:
            account_record.save(force_update=True)
        except Exception as e:
            print("ac: ", account_record.error_number)
            print("231: ", e)
    if request.method == "POST":
        # form = BookingForm (request.POST or None, initial='')
        # if form.is_valid():
        if "delete_import_journal" in request.POST:
            deleting_record = Import_Journal.objects.all()
            deleting_record.delete()
            context = dict(import_journal=journal_entrys)
            return redirect("import_journal_checking")
        if "final_import_journal" in request.POST:
            import_journal = Import_Journal.objects.last()
            last_number = import_journal.id
            last_number += 1  # otherwise the last one will not import
            i = 1
            for i in range(last_number):
                try:  # in case of not consecutively numbered id
                    account_record = Import_Journal.objects.get(pk=i)
                    final_journal = Journal(
                        amount=account_record.amount,
                        posting_key=account_record.posting_key,
                        debit_account=account_record.debit_account,
                        credit_account=account_record.credit_account,
                        date_field=account_record.date_field,
                        posting_text=account_record.posting_text,
                    )
                    final_journal.save()
                    account_record.delete()
                except:
                    pass
            context = dict(import_journal=journal_entrys)
            return render(request, "rewe/booking_import_journal_checking.html", context)
        if "convert_datev" in request.POST:
            for account_record in journal_entrys:
                date_raw = account_record.voucher_2
                year = int(date_raw % 10000)
                month = int(((date_raw % 1000000) - (date_raw % 10000)) / 10000)
                day = int(((date_now % 100000000) - (date_raw % 1000000)) / 1000000)
                account_record.date = str(day) + "." + str(month) + "" + str(year)
                # account_record.value_2 = ""
                account_record.save(force_update=True)
            return redirect("import_journal_checking")
        else:
            return HttpResponseBadRequest()
    else:
        context = {
            "import_journal": journal_entrys,
            # 'form'    : form
        }
        return render(request, "rewe/booking_import_journal_checking.html", context)
