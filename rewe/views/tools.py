from django.shortcuts import render
from django.http import HttpResponseBadRequest, FileResponse
from django.conf import settings
import django_excel as excel

from django.utils.translation import ugettext as _

from django.contrib.auth.decorators import login_required, permission_required

from reportlab.pdfgen import canvas
from reportlab.lib.units import cm
from reportlab.lib.pagesizes import A4, landscape
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.platypus import Frame, Paragraph, SimpleDocTemplate, PageBreak

from ..models import Posting_key
from .UploadFileForm import UploadFileForm

# reportlab / pdf
import io
import decimal

from core.models import Company_data
from ..models import Account

import logging

logger = logging.getLogger(__name__)


def pdf_view(content, directory, filename):
    # Create a file-like buffer to receive PDF data.
    buffer = io.BytesIO()

    # Create the PDF object, using the buffer as its "file."
    p = canvas.Canvas(buffer)
    p = canvas.Canvas(
        settings.BASE_DIR + settings.MEDIA_URL + directory + "/" + filename + ".pdf",
        pagesize=A4,
    )
    # Draw things on the PDF. Here's where the PDF generation happens.
    # See the ReportLab documentation for the full list of functionality.
    p.drawString(100, 100, content)

    # Close the PDF object cleanly, and we're done.
    p.showPage()

    p.save()
    # FileResponse sets the Content-Disposition header so that browsers
    # present the option to save the file.
    buffer.seek(0)
    return FileResponse(buffer, as_attachment=True, filename=filename)


def pdf_create(heading, content, directory, filename):
    # define style
    style = getSampleStyleSheet()
    # generate list which fills the page content
    story = []
    # generate content
    try:
        list(content)
    except Exception as e:
        print("Exception missing content in pdf_create ", e)
    # print("type content: ", type(content))
    if isinstance(content, list):
        print("Liste")
        story.append(Paragraph(heading, style["Heading1"]))
        print("heading: ", heading)
        print("content", content)
        for single_content in content:
            # print(f"Single content: {single_content}")
            story.append(Paragraph((single_content), style["BodyText"]))
        story.append(Paragraph("""© gyousei by bengoshi""", style["BodyText"]))
    else:
        story.append(Paragraph(heading, style["Heading1"]))
        # print("else")
        # print("content else: ", content)
        story.append(Paragraph(content, style["BodyText"]))
        story.append(Paragraph("""© gyousei by bengoshi""", style["BodyText"]))
    # story.append(PageBreak())
    # story.append(Paragraph('Überschrift 2',style['Heading2']))
    # story.append(Paragraph(''' Das ReportLab Toolkit steht unter einer freien
    # OpenSource Lizenz.''',style['BodyText']))
    # Anlegen des PDFs
    pdf = SimpleDocTemplate(
        settings.BASE_DIR + settings.MEDIA_URL + directory + "/" + filename + ".pdf",
        pagesize=A4,
    )
    pdf.build(story)


@permission_required("rewe.add_journal")
def upload(request):
    if request.method == "POST":
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            filehandle = request.FILES["file"]
            return excel.make_response(filehandle.get_sheet(), "csv")
        else:
            return HttpResponseBadRequest()
    else:
        form = UploadFileForm()
    return render(
        request,
        "rewe/tools_upload_form.html",
        {"form": form, "title": "Excel upload form", "header": "upload form"},
    )


@permission_required("rewe.view_journal")
def download(request):
    sheet = excel.pe.Sheet([[1, 2], [3, 4]])
    return excel.make_response(sheet, "csv")


def part_account_posting(account: int) -> list:
    company_data = Company_data.objects.last()
    liable_to_tax_on_purchases = company_data.liable_to_tax_on_purchases
    if liable_to_tax_on_purchases:
        if str(account).__len__() > 5:
            debit_account = str(account)[-1:-6:-1]
            debit_account = str(debit_account)[::-1]
            debit_account = int(debit_account)
            # print("debit account tool: ", debit_account)
            posting_key = str(account)[-6:-10:-1]
            posting_key = str(posting_key)[::-1]
            posting_key = int(posting_key)
            # print("posting key: ", posting_key)
        else:
            debit_account = account
            posting_key = 0
    if not liable_to_tax_on_purchases:
        debit_account = account
        posting_key = None
    return debit_account, posting_key


def tax_calc_posting_key(gross_amount: int, posting_key_number: int) -> list:
    try:
        posting_key = Posting_key.objects.get(pk=posting_key_number)
        rate = posting_key.tax_rate
        tax_amount = gross_amount * rate
        tax_amount = decimal.Decimal(tax_amount).quantize(
            decimal.Decimal("0.00"), rounding=decimal.ROUND_05UP
        )
        net_amount = gross_amount - tax_amount
        net_amount = decimal.Decimal(net_amount).quantize(
            decimal.Decimal("0.00"), rounding=decimal.ROUND_05UP
        )
    except:
        net_amount = gross_amount
        tax_amount = 0
    return net_amount, tax_amount


def tax_calc_summary_account(gross_amount: decimal, debit_account: int):
    if (Account.objects.filter(number=debit_account)) == "AM":
        rate = Account.objects.filter(number=debit_account)
