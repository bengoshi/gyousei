from django.shortcuts import render

from django.utils.translation import ugettext as _

from django.contrib.auth.decorators import login_required, permission_required

from ..models import Journal, Deleting_Journal, Account

from .tools import part_account_posting

import logging

logger = logging.getLogger(__name__)


@permission_required("rewe.view_journal")
def journal_overview(request):
    journal_entrys = Journal.objects.all()
    context = dict(journal=journal_entrys)
    return render(request, "rewe/reportings_journal_overview.html", context)


@permission_required("rewe.deleting_journal")
def deleting_journal_overview(request):
    journal_entrys = Deleting_Journal.objects.all()
    context = dict(journal=journal_entrys)
    return render(request, "rewe/reportings_deleting_journal_overview.html", context)


@permission_required("rewe.view_journal")
def trial_balance_sheet(request):
    """
    trial balance sheet - German: BWA
    this form is without VAT so all amounts are gross
    :param request: only request for report
    :return: report
    """
    # find out size of range
    # # find out booked accounts
    account_list = []
    # for i in range(last_number):
    for i in range(len(Journal.objects.all())):
        try:
            account_record = Journal.objects.get(pk=i)
            account_list.append(account_record.debit_account)
            account_list.append(account_record.credit_account)
        except:
            pass
    account_list = sorted(set(account_list))
    # account-list without opening-balance-accounts (between 9000 and 9999),
    # above this is sub-ledger account
    account_list_without_opening_balance = account_list[:]
    deleting_list = []
    for i in range(len(account_list_without_opening_balance)):
        if 9000 <= account_list_without_opening_balance[i] <= 9999:
            deleting_list.append(i)
    for i in reversed(range(len(deleting_list))):
        del account_list_without_opening_balance[deleting_list[i]]
    # find out opening balance sheet entries
    tba_line = []
    for i in range(len(account_list_without_opening_balance)):
        print("i", i)
        account_record_current = account_list_without_opening_balance[i]
        account_record_opening_sum_debit = 0
        account_record_opening_sum_credit = 0
        account_record_sum_debit = 0
        account_record_sum_credit = 0
        # find out opening balance amounts
        for account_record in Journal.objects.all():
            account_record_debit = account_record.debit_account
            account_record_credit = account_record.credit_account
            if account_record_debit == account_record_current:
                if 9000 <= account_record_credit <= 9999:
                    account_record_opening_sum_debit += account_record.amount
            if account_record_credit == account_record_current:
                if 9000 <= account_record_debit <= 9999:
                    account_record_opening_sum_credit += account_record.amount
        # find out sum of each balance amount
        for account_record in Journal.objects.all():
            account_record_debit = account_record.debit_account
            account_record_credit = account_record.credit_account
            if account_record_debit == account_record_current:
                if account_record_credit < 9000:
                    account_record_sum_debit += account_record.amount
            if account_record_credit == account_record_current:
                if account_record_debit < 9000:
                    account_record_sum_credit += account_record.amount
        tba_line.append(
            [
                account_record_current,
                account_record_opening_sum_debit,
                account_record_opening_sum_credit,
                account_record_sum_debit,
                account_record_sum_credit,
            ]
        )
    content = dict(opening_balance_list=tba_line)
    return render(request, "rewe/reportings_trial_balance_sheet.html", content)


@permission_required("rewe.view_journal")
def trial_balance_sheet_gross_vat(request):
    """
    trial balance sheet - German: BWA
    this form is gross and VAT is shown in extra accounts
    :param request: only request for report
    :return: report
    """
    # find out size of range
    # # find out booked accounts
    account_list = []
    posting_key_list = []
    for i in range(len(Journal.objects.all())):
        try:
            account_record = Journal.objects.get(pk=i)
            account_list.append(account_record.debit_account)
            account_record_debit = account_record.debit_account
            debit = part_account_posting(account_record_debit)
            account_record_debit = debit[0]
            account_record_posting_key = debit[1]
            account_list.append(account_record_debit)
            account_list.append(account_record.credit_account)
            if account_record_posting_key:
                posting_key_list.append(account_record_posting_key)
            else:
                account_record_functions = Account.objects.get(
                    number=account_record_debit
                )
                account_record_posting_key = account_record_functions.posting_key
                if account_record_posting_key:
                    posting_key_list.append(account_record_posting_key)
        except Exception as e:
            print("133 e: ", e)
        print("----")
        print("account_list: ", account_list)
        print("--")
        print("posting_key_list: ", posting_key_list)
    account_list = sorted(set(account_list))
    posting_key_list = [x for x in posting_key_list if x is not None]
    # posting_key_dict = {
    #     posting_key: 0 for posting_key in posting_key_list if posting_key is not None
    # }
    zero_list = []
    for i in posting_key_list:
        zero_list.append(0)
    posting_key_dict = {}
    posting_key_dict = dict(zip(posting_key_list, zero_list))
    # account-list without opening-balance-accounts (between 9000 and 9999),
    # above this is sub-ledger account
    account_list_without_opening_balance = account_list[:]
    deleting_list = []
    for i in range(len(account_list_without_opening_balance)):
        if 9000 <= account_list_without_opening_balance[i] <= 9999:
            deleting_list.append(i)
    for i in reversed(range(len(deleting_list))):
        del account_list_without_opening_balance[deleting_list[i]]
    # find out opening balance sheet entries
    tba_line = []
    for i in range(len(account_list_without_opening_balance)):
        account_record_current = account_list_without_opening_balance[i]
        account_record_opening_sum_debit = 0
        account_record_opening_sum_credit = 0
        account_record_sum_debit = 0
        account_record_sum_credit = 0
        # find out opening balance amounts
        for account_record in Journal.objects.all():
            account_record_debit = account_record.debit_account
            debit = part_account_posting(account_record_debit)
            account_record_debit = debit[0]
            account_record_posting_key = debit[1]
            account_record_credit = account_record.credit_account
            if account_record_debit == account_record_current:
                if 9000 <= account_record_credit <= 9999:
                    account_record_opening_sum_debit += account_record.amount
            if account_record_credit == account_record_current:
                if 9000 <= account_record_debit <= 9999:
                    account_record_opening_sum_credit += account_record.amount
        # find out sum of each balance amount
        for account_record in Journal.objects.all():
            account_record_debit = account_record.debit_account
            account_record_credit = account_record.credit_account
            if account_record_debit == account_record_current:
                if account_record_credit < 9000:
                    account_record_sum_debit += account_record.amount
                    try:
                        if (
                            account_record_posting_key
                            or account_record.tax_amount is not None
                        ):
                            posting_key_dict[
                                account_record_posting_key
                            ] += account_record.tax_amount
                    except Exception as e:
                        print("190: ", e)
                    try:
                        account_record_functions = Account.objects.get(
                            number=account_record_debit
                        )
                    except Exception as e:
                        print("185: ", e)
                    try:
                        account_record_posting_key = (
                            account_record_functions.posting_key
                        )
                        if account_record_functions:
                            posting_key_dict[
                                account_record_posting_key
                            ] += account_record.tax_amount
                    except Exception as e:
                        print("210: ", e)
                    # try:
                    #     account_record_main_function = (
                    #         account_record_functions.main_function
                    #     )
                    # except:
                    #     pass
                    # if account_record_main_function is "AM":
                    #     posting_key_dict[
                    #         account_record_posting_key
                    #     ] += account_record.tax_amount
            if account_record_credit == account_record_current:
                if account_record_debit < 9000:
                    account_record_sum_credit += account_record.amount
        # posting_key_dict
        tba_line.append(
            [
                account_record_current,
                account_record_opening_sum_debit,
                account_record_opening_sum_credit,
                account_record_sum_debit,
                account_record_sum_credit,
                posting_key_dict,
            ]
        )
    print(posting_key_dict)
    content = dict(opening_balance_list=tba_line, posting_key_list=posting_key_dict)
    return render(
        request, "rewe/reportings_trial_balance_sheet_gross_vat.html", content
    )


@permission_required("rewe.view_journal")
def account_sheet(request, account_number):
    account_sheet = []
    account_record_debit_sum = 0
    account_record_credit_sum = 0
    for i in range(len(Journal.objects.all())):
        try:
            account_record = Journal.objects.get(pk=i)
            account_record_debit = account_record.debit_account
            if account_record_debit == account_number:
                data = [
                    account_record.id,
                    account_record.date_field,
                    account_record.posting_key,
                    account_record.credit_account,
                    "",
                    account_record.amount,
                    account_record.posting_text,
                ]
                account_record_debit_sum += account_record.amount
            account_record_credit = account_record.credit_account
            if account_record_credit == account_number:
                data = [
                    account_record.id,
                    account_record.date_field,
                    account_record.posting_key,
                    "",
                    account_record.debit_account,
                    account_record.amount,
                    account_record.posting_text,
                ]
                account_record_credit_sum += account_record.amount
            account_sheet.append(data)
            account_total = account_record_debit_sum - account_record_credit_sum
            account_sum = [
                account_record_debit_sum,
                account_record_credit_sum,
                account_total,
            ]
        except:
            pass
    context = {"account_sheet": account_sheet, "account_sum": account_sum}
    return render(request, "rewe/reportings_account_sheet.html", context)
