from django.contrib.postgres.fields import ArrayField
from django.db import models
from django.urls import reverse
from django.utils.translation import gettext_lazy as _


from crm.models import Member

# Create your models here.
class Account(models.Model):
    number = models.DecimalField(
        max_digits=6,
        decimal_places=0,
        verbose_name=("account number"),
        null=True,
        blank=False,
        db_index=True,
    )
    name = models.CharField(
        max_length=120, verbose_name=("account name"), null=True, blank=False,
    )
    active_flag = models.BooleanField(null=False, blank=True)
    valid_from = models.DateField(verbose_name=("date_field"), null=True, blank=True,)
    summary_account = models.CharField(
        max_length=6, verbose_name=("summary account"), null=True, blank=True,
    )
    individual_flag = models.BooleanField(null=True, blank=True)
    main_function = models.CharField(
        max_length=6, verbose_name=("main function"), null=True, blank=True,
    )
    extra_function = models.CharField(
        max_length=6, verbose_name=("extra function"), null=True, blank=True,
    )
    function = models.CharField(
        max_length=6, verbose_name=("account function"), null=True, blank=True,
    )
    posting_key = models.DecimalField(
        max_digits=6,
        decimal_places=0,
        verbose_name=("posting key"),
        null=True,
        blank=True,
        db_index=True,
    )
    tax_rate = models.DecimalField(
        max_digits=4,
        decimal_places=2,
        verbose_name=("tax rate"),
        null=True,
        blank=False,
        db_index=True,
    )
    vat_data_sheet = models.CharField(
        max_length=2, verbose_name=("VAT data sheet"), null=True, blank=True,
    )
    date_last_booking = models.DateField(
        verbose_name="date last booking", null=True, blank=True,
    )
    kind_of_account = models.CharField(
        max_length=6, verbose_name=("kind of account"), null=True, blank=True,
    )
    vat_line_tax_on_issue = models.DecimalField(
        max_digits=4,
        decimal_places=2,
        verbose_name=("vat line - tax on issue"),
        null=True,
        blank=True,
        db_index=True,
    )
    vat_line_tax_on_payment = models.DecimalField(
        max_digits=4,
        decimal_places=2,
        verbose_name=("vat line - tax on payment"),
        null=True,
        blank=True,
        db_index=True,
    )
    factor2_flag = models.BooleanField(null=False, blank=True)
    factor2_percentage = models.DecimalField(
        max_digits=4,
        decimal_places=2,
        verbose_name=("factor2 percentage"),
        null=True,
        blank=True,
        db_index=True,
    )
    factor2_account1 = models.DecimalField(
        max_digits=6,
        decimal_places=0,
        verbose_name=("factor2 account No 1"),
        null=True,
        blank=True,
        db_index=True,
    )
    factor2_account2 = models.DecimalField(
        max_digits=6,
        decimal_places=0,
        verbose_name=("factor2 account No 2"),
        null=True,
        blank=True,
        db_index=True,
    )

    class Meta:
        app_label = "rewe"
        managed = True

    def __str__(self):
        return self.name


class Import_Account(models.Model):
    number = models.DecimalField(
        max_digits=6,
        decimal_places=0,
        verbose_name=("account number"),
        null=True,
        blank=False,
        db_index=True,
    )
    name = models.CharField(
        max_length=120, verbose_name=("account name"), null=True, blank=False,
    )
    active_flag = models.BooleanField(null=False, blank=True)
    valid_from = models.DateField(verbose_name="date_field", null=True, blank=True,)
    summary_account = models.CharField(
        max_length=6, verbose_name=("summary account"), null=True, blank=True,
    )
    individual_flag = models.BooleanField(null=True, blank=True)
    main_function = models.CharField(
        max_length=6, verbose_name=("main function"), null=True, blank=True,
    )
    extra_function = models.CharField(
        max_length=6, verbose_name=("extra function"), null=True, blank=True,
    )
    function = models.CharField(
        max_length=6, verbose_name=("account function"), null=True, blank=True,
    )
    posting_key = models.DecimalField(
        max_digits=6,
        decimal_places=0,
        verbose_name=("posting key"),
        null=True,
        blank=True,
        db_index=True,
    )
    tax_rate = models.DecimalField(
        max_digits=4,
        decimal_places=2,
        verbose_name=("tax rate"),
        null=True,
        blank=True,
        db_index=True,
    )
    vat_data_sheet = models.CharField(
        max_length=2, verbose_name=("VAT data sheet"), null=True, blank=True,
    )
    date_last_booking = models.DateField(
        verbose_name="date last booking", null=True, blank=True,
    )
    kind_of_account = models.CharField(
        max_length=6, verbose_name=("kind of account"), null=True, blank=True,
    )
    vat_line_tax_on_issue = models.DecimalField(
        max_digits=4,
        decimal_places=2,
        verbose_name=("vat line - tax on issue"),
        null=True,
        blank=True,
        db_index=True,
    )
    vat_line_tax_on_payment = models.DecimalField(
        max_digits=4,
        decimal_places=2,
        verbose_name=("vat line - tax on payment"),
        null=True,
        blank=True,
        db_index=True,
    )
    factor2_flag = models.BooleanField(null=False, blank=True)
    factor2_percentage = models.DecimalField(
        max_digits=4,
        decimal_places=2,
        verbose_name=("factor2 percentage"),
        null=True,
        blank=True,
        db_index=True,
    )
    factor2_account1 = models.DecimalField(
        max_digits=6,
        decimal_places=0,
        verbose_name=("factor2 account No 1"),
        null=True,
        blank=True,
        db_index=True,
    )
    factor2_account2 = models.DecimalField(
        max_digits=6,
        decimal_places=0,
        verbose_name=("factor2 account No 2"),
        null=True,
        blank=True,
        db_index=True,
    )

    class Meta:
        app_label = "rewe"
        managed = True


class Deleting_Account(models.Model):
    number = models.DecimalField(
        max_digits=6,
        decimal_places=0,
        verbose_name=("account number"),
        null=True,
        blank=False,
        db_index=True,
    )
    name = models.CharField(
        max_length=120, verbose_name=("account name"), null=True, blank=False,
    )
    active_flag = models.BooleanField(null=False, blank=True)
    valid_from = models.DateField(verbose_name="date_field", null=True, blank=True,)
    summary_account = models.CharField(
        max_length=6, verbose_name=("summary account"), null=True, blank=True,
    )
    individual_flag = models.BooleanField(null=True, blank=True)
    main_function = models.CharField(
        max_length=6, verbose_name=("main function"), null=True, blank=True,
    )
    extra_function = models.CharField(
        max_length=6, verbose_name=("extra function"), null=True, blank=True,
    )
    function = models.CharField(
        max_length=6, verbose_name=("account function"), null=True, blank=True,
    )
    posting_key = models.DecimalField(
        max_digits=6,
        decimal_places=0,
        verbose_name=("posting key"),
        null=True,
        blank=True,
        db_index=True,
    )
    tax_rate = models.DecimalField(
        max_digits=4,
        decimal_places=2,
        verbose_name=("tax rate"),
        null=True,
        blank=True,
        db_index=True,
    )
    vat_data_sheet = models.CharField(
        max_length=2, verbose_name=("VAT data sheet"), null=True, blank=True,
    )
    date_last_booking = models.DateField(
        verbose_name="date last booking", null=True, blank=True,
    )
    kind_of_account = models.CharField(
        max_length=6, verbose_name=("kind of account"), null=True, blank=True,
    )
    vat_line_tax_on_issue = models.DecimalField(
        max_digits=4,
        decimal_places=2,
        verbose_name=("vat line - tax on issue"),
        null=True,
        blank=True,
        db_index=True,
    )
    vat_line_tax_on_payment = models.DecimalField(
        max_digits=4,
        decimal_places=2,
        verbose_name=("vat line - tax on payment"),
        null=True,
        blank=True,
        db_index=True,
    )
    factor2_flag = models.BooleanField(null=False, blank=True)
    factor2_percentage = models.DecimalField(
        max_digits=4,
        decimal_places=2,
        verbose_name=("factor2 percentage"),
        null=True,
        blank=True,
        db_index=True,
    )
    factor2_account1 = models.DecimalField(
        max_digits=6,
        decimal_places=0,
        verbose_name=("factor2 account No 1"),
        null=True,
        blank=True,
        db_index=True,
    )
    factor2_account2 = models.DecimalField(
        max_digits=6,
        decimal_places=0,
        verbose_name=("factor2 account No 2"),
        null=True,
        blank=True,
        db_index=True,
    )
    edit_flag = models.CharField(
        max_length=1, verbose_name="edit flag", null=True, blank=False,
    )
    edit_date = models.DateTimeField(
        verbose_name="edit date", auto_now_add=True, editable=True, blank=True
    )

    class Meta:
        app_label = "rewe"
        managed = True


class Journal(models.Model):
    amount = models.DecimalField(
        max_digits=100,
        decimal_places=2,
        verbose_name="amount",
        null=False,
        blank=False,
    )
    tax_amount = models.DecimalField(
        max_digits=100,
        decimal_places=2,
        verbose_name="tax amount",
        null=True,
        blank=True,
    )
    net_amount = models.DecimalField(
        max_digits=100,
        decimal_places=2,
        verbose_name="net amount",
        null=True,
        blank=True,
    )
    posting_key = models.IntegerField(verbose_name="posting_key", null=True, blank=True)
    debit_account = models.IntegerField(
        verbose_name="debit_account", null=False, blank=False
    )
    credit_account = models.IntegerField(
        verbose_name="credit_account", null=False, blank=False
    )
    date_field = models.DateField(verbose_name="date_field", null=False, blank=False,)
    posting_text = models.CharField(
        max_length=120, verbose_name="posting_text", null=False, blank=False
    )
    member_number = models.IntegerField(
        verbose_name="member number", null=True, blank=True
    )
    account_number = models.IntegerField(
        verbose_name="account_number", null=True, blank=False
    )
    account_area = models.IntegerField(
        verbose_name="account_area", null=True, blank=True
    )
    voucher_1 = models.CharField(
        max_length=25, verbose_name="voucher-1", null=True, blank=True
    )
    voucher_2 = models.CharField(
        max_length=25, verbose_name="voucher-2", null=True, blank=True
    )
    payment_date = models.DateField(verbose_name="payment date", null=True, blank=True)
    payed_flag = models.BooleanField(null=True, blank=True)
    external_document_number = models.CharField(
        max_length=25, verbose_name="external document number", null=True, blank=True
    )
    memo = models.TextField(verbose_name="memo", null=True, blank=True)
    event_number = models.IntegerField(
        verbose_name="event number", null=True, blank=True
    )
    clearing_number = models.IntegerField(
        verbose_name="clearing number", null=True, blank=True
    )

    def get_absolute_url(self):
        return reverse("rewe>>", kwargs={"id": self.id})


class Deleting_Journal(models.Model):
    amount = models.DecimalField(
        max_digits=100,
        decimal_places=2,
        verbose_name="amount",
        null=False,
        blank=False,
    )
    tax_amount = models.DecimalField(
        max_digits=100,
        decimal_places=2,
        verbose_name="tax amount",
        null=True,
        blank=True,
    )
    net_amount = models.DecimalField(
        max_digits=100,
        decimal_places=2,
        verbose_name="net amount",
        null=True,
        blank=True,
    )
    posting_key = models.IntegerField(verbose_name="posting_key", null=True, blank=True)
    debit_account = models.IntegerField(
        verbose_name="debit_account", null=False, blank=False
    )
    credit_account = models.IntegerField(
        verbose_name="credit_account", null=False, blank=False
    )
    date_field = models.DateField(verbose_name="date_field", null=False, blank=False)
    posting_text = models.CharField(
        max_length=120, verbose_name="posting_text", null=False, blank=False
    )
    member_number = models.IntegerField(
        verbose_name="member number", null=True, blank=True
    )
    account_number = models.IntegerField(
        verbose_name="account_number", null=True, blank=True
    )
    account_area = models.IntegerField(
        verbose_name="account_area", null=True, blank=True
    )
    voucher_1 = models.CharField(
        max_length=25, verbose_name="voucher-1", null=True, blank=True
    )
    voucher_2 = models.CharField(
        max_length=25, verbose_name="voucher-2", null=True, blank=True
    )
    payment_date = models.DateField(verbose_name="payment date", null=True, blank=True)
    payed_flag = models.BooleanField(null=True, blank=True)
    external_document_number = models.CharField(
        max_length=25, verbose_name="external document number", null=True, blank=True
    )
    memo = models.TextField(verbose_name="memo", null=True, blank=True)
    event_number = models.IntegerField(
        verbose_name="event number", null=True, blank=True
    )
    clearing_number = models.IntegerField(
        verbose_name="clearing number", null=True, blank=True
    )
    edit_flag = models.CharField(
        max_length=1, verbose_name="edit flag", null=True, blank=False,
    )
    edit_date = models.DateTimeField(
        verbose_name="edit date", auto_now_add=True, editable=True, blank=True
    )
    edit_flag = models.CharField(
        max_length=1, verbose_name="edit flag", null=True, blank=False,
    )
    edit_date = models.DateTimeField(
        verbose_name="edit date", auto_now_add=True, editable=True, blank=True
    )


class Import_Journal(models.Model):
    amount = models.CharField(
        max_length=120,
        # max_digits=100,
        # decimal_places=2,
        # verbose_name='amount',
        null=False,
        blank=False,
    )
    tax_amount = models.DecimalField(
        max_digits=100,
        decimal_places=2,
        verbose_name="tax amount",
        null=True,
        blank=True,
    )
    net_amount = models.DecimalField(
        max_digits=100,
        decimal_places=2,
        verbose_name="net amount",
        null=True,
        blank=True,
    )
    debit_flag = models.CharField(
        max_length=1, verbose_name="debit_flag", null=True, blank=True
    )
    posting_key = models.IntegerField(verbose_name="posting_key", null=True, blank=True)
    debit_account = models.IntegerField(
        verbose_name="debit_account", null=False, blank=False
    )
    credit_account = models.IntegerField(
        verbose_name="credit_account", null=False, blank=False
    )
    date_field = models.DateField(verbose_name="date_field", null=True, blank=True)
    posting_text = models.CharField(
        max_length=120, verbose_name="posting_text", null=False, blank=False
    )
    member_number = models.IntegerField(
        verbose_name="member number", null=True, blank=True
    )
    account_number = models.IntegerField(
        verbose_name="account_number", null=True, blank=True
    )
    account_area = models.IntegerField(
        verbose_name="account_area", null=True, blank=True
    )
    voucher_1 = models.CharField(
        max_length=25, verbose_name="voucher-1", null=True, blank=True
    )
    voucher_2 = models.CharField(
        max_length=25, verbose_name="voucher-2", null=True, blank=True
    )
    payment_date = models.DateField(verbose_name="payment date", null=True, blank=True)
    payed_flag = models.BooleanField(null=True, blank=True)
    external_document_number = models.CharField(
        max_length=25, verbose_name="external document number", null=True, blank=True
    )
    memo = models.TextField(verbose_name="memo", null=True, blank=True)
    event_number = models.IntegerField(
        verbose_name="event number", null=True, blank=True
    )
    clearing_number = models.IntegerField(
        verbose_name="clearing number", null=True, blank=True
    )
    # error_number = ArrayField(
    #     ArrayField(models.CharField(max_length=6, blank=True), size=2,), size=2,
    # )
    # error_no = models.JSONFieldb(null=True)


class Debit_position(models.Model):
    month = models.IntegerField(verbose_name="month", null=False, blank=False)
    year = models.IntegerField(verbose_name="year", null=True, blank=True)
    creation_date = models.DateField(verbose_name="creation date")
    part_or_complete = models.BooleanField(
        verbose_name="part_or_complete", null=True, blank=True
    )  # if true it is complete if false it is only a part of all members
    sum_of_debit_positions = models.DecimalField(
        max_digits=1000,
        decimal_places=2,
        verbose_name="sum of debit positions",
        null=False,
        blank=False,
    )
    debit_position_file_pdf = models.FileField(upload_to="debit_position/")
    debit_position_file_csv = models.FileField(upload_to="debit_position/")
    uploaded_at = models.DateTimeField(auto_now_add=True)


# sepa-file muss in ein Lastschrifteinzug


class Direct_debit_journal(models.Model):
    month = models.IntegerField(verbose_name="month", null=False, blank=False)
    year = models.IntegerField(verbose_name="year", null=True, blank=True)
    creation_date = models.DateField(verbose_name="creation date")
    origin = models.BooleanField(
        verbose_name="origin", null=True, blank=True
    )  # if true - origin is part of debit position, if false it is part of payment modul
    sum_of_debit_positions = models.DecimalField(
        max_digits=1000,
        decimal_places=2,
        verbose_name="sum of debit positions",
        null=False,
        blank=False,
    )
    direct_debit_file_pdf = models.FileField(upload_to="sepa_debit_position/")
    sepa_file = models.FileField(upload_to="sepa_debit_position/")
    uploaded_at = models.DateTimeField(auto_now_add=True)


class Debit_position_journal(Journal):
    # depricated!
    def __str__(self):
        return self


class Debit_position_jrl(models.Model):
    amount = models.DecimalField(
        max_digits=100,
        decimal_places=2,
        verbose_name="amount",
        null=False,
        blank=False,
    )
    tax_amount = models.DecimalField(
        max_digits=100,
        decimal_places=2,
        verbose_name="tax amount",
        null=True,
        blank=True,
    )
    net_amount = models.DecimalField(
        max_digits=100,
        decimal_places=2,
        verbose_name="net amount",
        null=True,
        blank=True,
    )
    posting_key = models.IntegerField(verbose_name="posting_key", null=True, blank=True)
    debit_account = models.IntegerField(
        verbose_name="debit_account", null=False, blank=False
    )
    credit_account = models.IntegerField(
        verbose_name="credit_account", null=False, blank=False
    )
    date_field = models.DateField(verbose_name="date_field", null=False, blank=False)
    posting_text = models.CharField(
        max_length=120, verbose_name="posting_text", null=False, blank=False
    )
    member_number = models.IntegerField(
        verbose_name="member number", null=True, blank=True
    )
    # member_number = models.ForeignKey(Member, on_delete=models.SET_NULL, null=True)
    account_number = models.IntegerField(
        verbose_name="account_number", null=True, blank=False
    )
    account_area = models.IntegerField(
        verbose_name="account_area", null=True, blank=True
    )
    voucher_1 = models.CharField(
        max_length=25, verbose_name="voucher-1", null=True, blank=True
    )
    voucher_2 = models.CharField(
        max_length=25, verbose_name="voucher-2", null=True, blank=True
    )
    payment_date = models.DateField(verbose_name="payment date", null=True, blank=True)
    payed_flag = models.BooleanField(null=True, blank=True)
    external_document_number = models.CharField(
        max_length=25, verbose_name="external document number", null=True, blank=True
    )
    memo = models.TextField(verbose_name="memo", null=True, blank=True)
    event_number = models.IntegerField(
        verbose_name="event number", null=True, blank=True
    )
    clearing_number = models.IntegerField(
        verbose_name="clearing number", null=True, blank=True
    )


class Posting_key(models.Model):
    tax_key = models.IntegerField(
        verbose_name=_("tax key"), null=True, blank=False, db_index=True,
    )
    name = models.CharField(
        max_length=120, verbose_name=_("account name"), null=True, blank=False,
    )
    active_flag = models.BooleanField(null=False, blank=True)
    individual_flag = models.BooleanField(null=False, blank=True)
    valid_from = models.DateField(verbose_name=_("date_field"), null=True, blank=True,)
    tax_rate = models.DecimalField(
        max_digits=4,
        decimal_places=2,
        verbose_name=_("tax rate"),
        null=True,
        blank=True,
    )
    travel_expenses = models.BooleanField(null=True, blank=True)
    vat_account = models.IntegerField(
        verbose_name=_("VAT account"), null=True, blank=True,
    )
    input_vat_account = models.IntegerField(
        verbose_name=_("input VAT account"), null=True, blank=True,
    )
    vat_account_unmatured = models.IntegerField(
        verbose_name=_("VAT account unmatured"), null=True, blank=True,
    )
    cash_discount_account = models.IntegerField(
        verbose_name=_("cash discount account"), null=True, blank=True,
    )
    summary_account = models.IntegerField(
        verbose_name=_("summary account"), null=True, blank=True,
    )
    vat_code_number_accrual_basis_tax_payer = models.IntegerField(
        verbose_name=_("VAT code number accrual basis tax payer"),
        null=True,
        blank=True,
    )
    vat_code_number_actual_basis_tax_payer = models.IntegerField(
        verbose_name=_("VAT code number actual basis tax payer"), null=True, blank=True,
    )
    factor2_flag = models.BooleanField(null=False, blank=True, default=False)
    factor2_percentage = models.DecimalField(
        max_digits=4,
        decimal_places=2,
        verbose_name=_("factor2 percentage"),
        null=True,
        blank=True,
    )
    factor2_account1 = models.IntegerField(
        verbose_name=_("factor2 account 1"), null=True, blank=True,
    )
    factor2_account2 = models.IntegerField(
        verbose_name=_("factor2 account 2"), null=True, blank=True,
    )


class Import_Posting_key(models.Model):
    tax_key = models.IntegerField(
        verbose_name=_("tax key"), null=True, blank=False, db_index=True,
    )
    name = models.CharField(
        max_length=120, verbose_name=_("account name"), null=True, blank=False,
    )
    active_flag = models.BooleanField(null=False, blank=True)
    individual_flag = models.BooleanField(null=False, blank=True)
    valid_from = models.DateField(verbose_name=_("date_field"), null=True, blank=True,)
    tax_rate = models.DecimalField(
        max_digits=4,
        decimal_places=2,
        verbose_name=_("tax rate"),
        null=True,
        blank=True,
    )
    travel_expenses = models.BooleanField(null=True, blank=True)
    vat_account = models.IntegerField(
        verbose_name=_("VAT account"), null=True, blank=True,
    )
    input_vat_account = models.IntegerField(
        verbose_name=_("input VAT account"), null=True, blank=True,
    )
    vat_account_unmatured = models.IntegerField(
        verbose_name=_("VAT account unmatured"), null=True, blank=True,
    )
    cash_discount_account = models.IntegerField(
        verbose_name=_("cash discount account"), null=True, blank=True,
    )
    summary_account = models.IntegerField(
        verbose_name=_("summary account"), null=True, blank=True,
    )
    vat_code_number_accrual_basis_tax_payer = models.IntegerField(
        verbose_name=_("VAT code number accrual basis tax payer"),
        null=True,
        blank=True,
    )
    vat_code_number_actual_basis_tax_payer = models.IntegerField(
        verbose_name=_("VAT code number actual basis tax payer"), null=True, blank=True,
    )
    factor2_flag = models.BooleanField(null=False, blank=True, default=False)
    factor2_percentage = models.DecimalField(
        max_digits=4,
        decimal_places=2,
        verbose_name=_("factor2 percentage"),
        null=True,
        blank=True,
    )
    factor2_account1 = models.IntegerField(
        verbose_name=_("factor2 account 1"), null=True, blank=True,
    )
    factor2_account2 = models.IntegerField(
        verbose_name=_("factor2 account 2"), null=True, blank=True,
    )


class Deleting_Posting_key(models.Model):
    tax_key = models.IntegerField(
        verbose_name=_("tax key"), null=True, blank=False, db_index=True,
    )
    name = models.CharField(
        max_length=120, verbose_name=_("account name"), null=True, blank=False,
    )
    active_flag = models.BooleanField(null=False, blank=True)
    individual_flag = models.BooleanField(null=False, blank=True)
    valid_from = models.DateField(verbose_name=_("date_field"), null=True, blank=True,)
    tax_rate = models.DecimalField(
        max_digits=4,
        decimal_places=2,
        verbose_name=_("tax rate"),
        null=True,
        blank=True,
    )
    travel_expenses = models.BooleanField(null=True, blank=True)
    vat_account = models.IntegerField(
        verbose_name=_("VAT account"), null=True, blank=True,
    )
    input_vat_account = models.IntegerField(
        verbose_name=_("input VAT account"), null=True, blank=True,
    )
    vat_account_unmatured = models.IntegerField(
        verbose_name=_("VAT account unmatured"), null=True, blank=True,
    )
    cash_discount_account = models.IntegerField(
        verbose_name=_("cash discount account"), null=True, blank=True,
    )
    summary_account = models.IntegerField(
        verbose_name=_("summary account"), null=True, blank=True,
    )
    vat_code_number_accrual_basis_tax_payer = models.IntegerField(
        verbose_name=_("VAT code number accrual basis tax payer"),
        null=True,
        blank=True,
    )
    vat_code_number_actual_basis_tax_payer = models.IntegerField(
        verbose_name=_("VAT code number actual basis tax payer"), null=True, blank=True,
    )
    factor2_flag = models.BooleanField(null=False, blank=True, default=False)
    factor2_percentage = models.DecimalField(
        max_digits=4,
        decimal_places=2,
        verbose_name=_("factor2 percentage"),
        null=True,
        blank=True,
    )
    factor2_account1 = models.IntegerField(
        verbose_name=_("factor2 account 1"), null=True, blank=True,
    )
    factor2_account2 = models.IntegerField(
        verbose_name=_("factor2 account 2"), null=True, blank=True,
    )
    edit_flag = models.CharField(
        max_length=1, verbose_name="edit flag", null=True, blank=False,
    )
    edit_date = models.DateTimeField(
        verbose_name="edit date", auto_now_add=True, editable=True, blank=True
    )
