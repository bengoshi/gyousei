from django.urls import path
from django.conf.urls import url, include
from .views import (
    account_overview,
    journal_overview,
    deleting_journal_overview,
    journal_booking,
    accounting_record_edit,
    account_create,
    account_edit,
    trial_balance_sheet,
    trial_balance_sheet_gross_vat,
    upload,
    download,
    import_data,
    import_datev,
    import_journal_checking,
    import_account_checking,
    import_accounts,
    posting_key_import,
    posting_key_import_checking,
    posting_key_overview,
    account_sheet,
    debit_position,
    debit_position_checking,
)

# AccountOverview
# account_list

# app_name = 'rewe'
urlpatterns = [
    url(r"^i18n/", include("django.conf.urls.i18n")),
    # path('ao/', AccountOverview.as_view()),
    path("journal_booking", journal_booking, name="journal_booking"),
    path("account_create", account_create, name="account_create"),
    path("<int:pk>/", accounting_record_edit, name="accounting_record_edit"),
    path("account_sheet/<int:account_number>/", account_sheet, name="account_sheet"),
    path("account_overview", account_overview, name="account_overview"),  # deprecated
    path("account/<int:pk>/", account_edit, name="account_edit"),
    path("journal", journal_overview, name="journal_overview"),
    path(
        "deleting_journal", deleting_journal_overview, name="deleting_journal_overview"
    ),
    path("tbs", trial_balance_sheet_gross_vat, name="trial_balance_sheet"),
    path("upload", upload, name="upload"),
    path("download", download, name="download"),
    path("import_accounting_records", import_data, name="import_data"),
    path("import_accounting_records_datev", import_datev, name="import_datev"),
    path("import_accounts", import_accounts, name="import_accounts"),
    # path('handson', handson, name='handson'),
    path(
        "import_journal_checking",
        import_journal_checking,
        name="import_journal_checking",
    ),
    path(
        "import_account_checking",
        import_account_checking,
        name="import_account_checking",
    ),
    path("posting_key_import", posting_key_import, name="posting_key_import"),
    path(
        "posting_key_import_checking",
        posting_key_import_checking,
        name="posting_key_import_checking",
    ),
    path("posting_key_overview", posting_key_overview, name="posting_key_overview"),
    # path('account_sheet', account_sheet, name='account_sheet'),
    path("debit_position", debit_position, name="debit_position"),
    path(
        "debit_position_checking",
        debit_position_checking,
        name="debit_position_checking",
    ),
    # path('', account_list_view.as_view(), name='account_list_view'),
]
