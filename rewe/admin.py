from django.contrib import admin

# Register your models here.
from .models import Account,\
    Journal,\
    Deleting_Journal,\
    Import_Journal,\
    Import_Account,\
    Deleting_Account,\
    Debit_position,\
    Debit_position_jrl,\
    Direct_debit_journal,\
    Import_Posting_key,\
    Posting_key

admin.site.register(Account)
admin.site.register(Journal)
admin.site.register(Deleting_Journal)
admin.site.register(Import_Journal)
admin.site.register(Import_Account)
admin.site.register(Deleting_Account)
admin.site.register(Debit_position)
admin.site.register(Debit_position_jrl)
admin.site.register(Direct_debit_journal)
admin.site.register(Import_Posting_key)
admin.site.register(Posting_key)