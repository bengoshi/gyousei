from datetime import datetime

def bigger_or_equal_than_today(month, year):
    today = datetime.now()
    today_month = today.month
    today_year = today.year
    try:
        month = int(month)
    except:
        print("Crash in bigger_or_equal_than_today when transforming month to integer.")
    try:
        year = int(year)
    except:
        print("Crash in bigger_or_equal_than_today when transforming year to integer.")
    if year >= today_year:
        flag = True
        if month >= today_month:
            flag = True
    else:
        flag = False
    return(flag)

def bigger_than_today(month, year):
    today = datetime.now()
    today_month = today.month
    today_year = today.year
    try:
        month = int(month)
    except:
        print("Crash in bigger_than_today when transforming month to integer.")
    try:
        year = int(year)
    except:
        print("Crash in bigger_than_today when transforming year to integer.")
    if year > today_year:
        flag = True
        if month > today_month:
            flag = True
    else:
        flag = False
    return(flag)

def smaller_or_equal_than_today(month, year):
    today = datetime.now()
    today_month = today.month
    today_year = today.year
    try:
        month = int(month)
    except:
        print("Crash in smaller_or_equal_than_today when transforming month to integer.")
    try:
        year = int(year)
    except:
        print("Crash in smaller_or_equal_than_today when transforming year to integer.")
    if year <= today_year:
        flag = True
        if month <= today_month:
            flag = True
    else:
        flag = False
    return(flag)

def smaller_than_today(month, year):
    today = datetime.now()
    today_month = today.month
    today_year = today.year
    try:
        month = int(month)
    except:
        print("Crash in smaller_than_today when transforming month to integer.")
    try:
        year = int(year)
    except:
        print("Crash in smaller_than_today when transforming year to integer.")
    if year < today_year:
        flag = True
        if month < today_month:
            flag = True
    else:
        flag = False
    return(flag)
